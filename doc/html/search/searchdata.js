var indexSectionsWithContent =
{
  0: "acdefgijlmnoprstvwxyÉ",
  1: "acfjtvw",
  2: "acfjmtvw",
  3: "acdefgijlmnoprstvw",
  4: "acdfijmnprtwxy",
  5: "cmÉ",
  6: "acmnrv",
  7: "a"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "pages"
};

var indexSectionLabels =
{
  0: "Tout",
  1: "Classes",
  2: "Fichiers",
  3: "Fonctions",
  4: "Variables",
  5: "Énumérations",
  6: "Valeurs énumérées",
  7: "Pages"
};

