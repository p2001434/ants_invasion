/**
 * @file AffichageConsole.cpp
 * @brief Définition des fonctions et procédures de AffichageConsole.h
 * @date 2023-04-30
 */

#include "AffichageConsole.h"
#include <iostream>
#include <unistd.h>
// #include <conio.h>
#include <string>
#include <vector>

using namespace std;

void txtAff(WinTXT &win, Jeu &jeu)
{
    Terrain ter = jeu.Jeu_Terrain;
    unsigned int i, j, k;
    vector<Fourmi> Tab_fourmiRestante;
    Tab_fourmiRestante = ter.Fourmiliere.tabFourmi;

    Vecteur pos_fourmi_test;

    for (i = 0; i < ter.DIMX; i++)
    {
        for (j = 0; j < ter.DIMY; j++)
        {
            unsigned int état = ter.tab_Terrain_Cellule[i * ter.DIMY + j].getC_Etat();
            Vecteur pos_cellule = ter.tab_Terrain_Cellule[i * ter.DIMY + j].getPositionCellule();
            int nb_fourmis = ter.Fourmiliere.getNombreFourmis();
            for (k = 0; k < (unsigned int)nb_fourmis; k++)
            {
                pos_fourmi_test = ter.Fourmiliere.tabFourmi[k].getPosition();
                win.print(pos_fourmi_test.x, pos_fourmi_test.y, '8');
            }
            switch (état)
            {
            case 0:
                win.print(pos_cellule.x, pos_cellule.y, '.');
                break;

            case 1:
                win.print(pos_cellule.x, pos_cellule.y, 'o');
                break;

            case 2:
                win.print(pos_cellule.x, pos_cellule.y, '#');
                break;

            case 3:
                win.print(pos_cellule.x, pos_cellule.y, 'C');
                break;
            }
        }
    }
    win.draw();
}

/*void txtAff(WinTXT & win, Jeu & jeu)
{
   Terrain ter = jeu.Jeu_Terrain;
   unsigned int i;

   for(i=0;i<ter.tab_Terrain_Cellule.size();i++)
   {
       Vecteur pos_cell =ter.tab_Terrain_Cellule[i].getPositionCellule();
       Vecteur pos_fourmi=ter.Fourmiliere.tabFourmi[i].getPosition();

       if(pos_fourmi.x==pos_cell.y && pos_fourmi.y==pos_cell.y)
       {
            win.print(pos_fourmi.x,pos_fourmi.y,'8');
       }

       else if(ter.tab_Terrain_Cellule[i].getC_Etat()==0)
       {
           Vecteur pos_cell=pos_cell;
           win.print(pos_cell.x,pos_cell.y,'.');
       }
       else if (ter.tab_Terrain_Cellule[i].getC_Etat()==1)
       {
           Vecteur pos_nourriture=pos_cell;
           win.print(pos_nourriture.x,pos_nourriture.y,'o');
       }
       else if (ter.tab_Terrain_Cellule[i].getC_Etat()==2)
       {
           Vecteur pos_mur=pos_cell;
           win.print(pos_mur.x,pos_mur.y,'#');
       }
       else if (ter.tab_Terrain_Cellule[i].getC_Etat()==3)
       {cellule
           Vecteur pos_Fourmiliere=pos_cell;
           win.print(pos_Fourmiliere.x,pos_Fourmiliere.x,'C');
       }
       else {cout<<"il y a un problème"<<endl;}
   }
   win.draw();
}*/

void txtBoucle(Jeu &jeu)
{

    WinTXT win(jeu.Jeu_Terrain.DIMX, jeu.Jeu_Terrain.DIMY);
    // bool ok = true;
    // int c;

    int unsigned i;
    i = 20;

    do
    {
        txtAff(win, jeu);
#ifdef _WIN32
        Sleep(1000000);
#else
        usleep(1000000);
#endif // WIN32
        /* code */
        jeu.ActualisationPartie();
        termClear();

        /*int c = win.getCh();
        jeu.ActionClavier(c)
        if()*/
    } while (i > 0);
}

/*AffichageConsole::AffichageConsole()
{

    cout << endl
         << "debut de l'affichage console" << endl;
    BoucleTerminal();
}*/

/*void AffichageConsole::BoucleTerminal()
{
    bool continuer = true;
    while (continuer)
    {
        AffichageTerminal();
        sleep(2);
        cout << endl
             << "voulez vous continuer?" << endl;
        cin >> continuer;
        cout << endl;
    }
}*/

/*void AffichageConsole::AffichageTerminal()
{

    vector<Fourmi> emplacementFourmi;
    unsigned int j = 0;
    bool case_F = true;
    emplacementFourmi = Jeu_Console.Jeu_Terrain.terrain_Colonie.tabFourmi;
    for (unsigned int i = 0; i < Jeu_Console.Jeu_Terrain.tab_Terrain_Cellule.size(); i++)
    {
        if (i % Jeu_Console.Jeu_Terrain.DIMX == 0)
        {
            cout << endl;
        }
        case_F = true;
        j = 0;
        while (j < emplacementFourmi.size() && case_F)
        {
            if (emplacementFourmi[j].getPosition() == Jeu_Console.Jeu_Terrain.tab_Terrain_Cellule[i].position_Cellule)
            {
                cout << "F";
                emplacementFourmi.erase(emplacementFourmi.begin() + j);
                case_F = false;
            }
            else
            {
                j++;
            }
        }
        if (case_F)
        {
            switch ((int)Jeu_Console.Jeu_Terrain.tab_Terrain_Cellule[i].ÉtatCellule())
            {
            case 0:
                cout << "X";
                break;
            case 1:
                cout << "N";
                break;
            case 2:
                cout << "M";
                break;
            case 3:
                cout << "C";
                break;
            default:
                break;
            }
        }
    }
}
*/