/**
 * @brief Module définissant l'AffichageConsole
 * @file AffichageConsole.h
 * @date 2023/03/06
 */

#ifndef _AFFICHAGECONSOLE_H
#define _AFFICHAGECONSOLE_H

#include "../core/Jeu.h"
#include "winTxt.h"

/**
 * @brief un AffichageConsole est composé de d'un Jeu a afficher
 */

  
  //////////////////////////////////////////////////////

    /**
     * @fn void txtAff(WinTXT & win, Jeu & jeu)
     * @brief Procédure permettant l'affichage du jeu dans une fenetre (terminal)
     * @param[in,out] win : fenêtre du jeu de type WinTxt
     * @param[in,out] jeu : le jeu de type Jeu
     */
  void txtAff(WinTXT & win, Jeu & jeu);

  //////////////////////////////////////////////////////

  /**
     * @fn void txtBoucle(Jeu & jeu)
     * @brief Procédure permettant la boucle d'évènement du jeu
     * @param[in,out] jeu : le jeu de type Jeu 
     */
  void txtBoucle(Jeu & jeu);

  //////////////////////////////////////////////////////


#endif