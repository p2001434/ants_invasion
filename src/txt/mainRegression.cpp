#include <iostream>
#include <cassert>
#include <algorithm>
#include <random>
#include "../core/Jeu.h"

using namespace std;

int main(void){
    srand(time(NULL));
    Jeu Jeu_de_test(15,25);
    Jeu_de_test.TestDeRegression();
    return 0;
}