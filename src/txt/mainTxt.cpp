#include <iostream>
#include <cassert>
#include "../txt/winTxt.h"
#include "../txt/AffichageConsole.h"

using namespace std;

int main ( int argc, char** argv ) {
	assert(4 > 3);
    termClear();
	unsigned int XX=35;
	unsigned int YY=20;
	Jeu JeuTxt(XX,YY);
	txtBoucle(JeuTxt);
    termClear();
	return 0;
}
