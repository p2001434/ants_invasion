/**
 * @file Colonie_Fourmis.cpp
 * @brief Définition des fonctions et procédures de Colonie_Fourmis.h
 * @date 2023-04-30
 */
#include "Colonie_Fourmis.h"

Colonie::Colonie()
{
    unsigned int ZERO = 0;
    position_Colonie.x = ZERO;
    position_Colonie.y = ZERO;
    tabFourmi.clear();
    compteur_nourriture = ZERO;
}

Colonie::Colonie(const Vecteur &point_apparition, const unsigned int nombre_fourmi)
{
    nombre_fourmis = nombre_fourmi;
    position_Colonie = point_apparition;
    if (nombre_fourmi != 0)
    {
        tabFourmi.resize(nombre_fourmis, position_Colonie);
    }
    else
    {
        tabFourmi.clear();
    }
    compteur_nourriture = 0;
}

void Colonie::setNombreFourmis(const unsigned int nv_nb_fourmis)
{
    nombre_fourmis = nv_nb_fourmis;
    tabFourmi.resize(nombre_fourmis, position_Colonie);
}

const unsigned int Colonie::getNombreFourmis()const
{
    return nombre_fourmis;
}

void Colonie::ajouteFourmi()
{
    tabFourmi.push_back(position_Colonie);
}

void Colonie::enleverFourmi(const unsigned int numero_fourmi)
{
    tabFourmi.erase(tabFourmi.begin() + numero_fourmi);
}

const Vecteur Colonie::getPosition() const
{
    return position_Colonie;
}