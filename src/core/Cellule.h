/**
 * @brief Module définissant une Cellule
 * @file Cellule.h
 * @date 2023/03/06
 */

#ifndef _CELLULE_H
#define _CELLULE_H

#include "Vecteur.h"

/**
 * @brief Une Cellule est composée d'un entier positif C_Etat et d'un enum de type Etat [0:vide,1:nourriture,2:mur,3:colonie].<br>
 * Les deux entiers positifs pheromone_maison et pheromone_nourriture sont des compteurs de phéromones. <br>
 * Le Vecteur position_Cellule (x,y) indique la position de la Cellule. 
 *
 */
class Cellule
{

private:
/**
 * @brief Valeur représentant l'état de la Cellule
 * 
 */
    unsigned int C_Etat;       
/**
 * @brief Valeur représentant la quantité de Phéromone maison de la Cellule
 * 
 */
    unsigned int pheromone_maison;  
/**
 * @brief Valeur représentant la quantité de Phéromone nourriture de la Cellule
 */
    unsigned int pheromone_nourriture; 
/**
 * @brief Vecteur représentant les coordonnées de la Cellule
 */
    Vecteur position_Cellule;
/**
 * @brief Valeur representant la Couleur de la Cellule
 */
    unsigned int couleur;

    /**
     * @name Configuration des valeurs de base :
     * @{
     */
     
     /**
      * @brief Quantité de phéromone maison lorsque qu'une Fourmi passe pour la première fois sans nourriture
      */
        static const unsigned int INIT_PHE_MAISON = 50; 
    /**
     * @brief Quantité de phéromone nourriture lorsque qu'une Fourmi passe pour la première fois avec de la nourriture
     */
        static const unsigned int INIT_PHE_NOURRITURE = 100;
    /**
     * @brief Quantité de phéromone maison ajouté lorsque qu'une Fourmi passe sans porter de nourriture
     */
        static const unsigned int AJOUT_PHE_MAISON = 5;
    /**
     * @brief Quantité de phéromone nourriture ajouté lorsque qu'une Fourmi passe avec de la nourriture
     */
        static const unsigned int AJOUT_PHE_NOURRITURE=8 ;

    /**
     * @}
     */
        
    //////////////////////////////////////////////////////


public:
    //////////////////////////////////////////////////////

    /**
    * @brief enum Etat d'une cellule. VIDE = 0, NOURRITURE = 1, MUR = 2, COLONIE = 3
    */

    enum Etat {
        VIDE = 0, /**< enum de type Etat de valeur 0, représente le VIDE */
        NOURRITURE = 1, /**< enum de type Etat de valeur 1, représente la NOURRITURE*/
        MUR = 2, /**< enum de type Etat de valeur 2, représente le MUR*/
        COLONIE = 3 /**< enum de type Etat de valeur 3, représente la COLONIE */
    };



    /**
     * @fn Cellule()
     * @brief Initialise une cellule vide (C_Etat à VIDE et les compteurs de phéromones à 0).
     */
    Cellule();

    //////////////////////////////////////////////////////

    /**
     *@fn const unsigned int getC_Etat()const
     *@brief Permet d'obtenir l'état d'une Cellule.
     *@return Retourne un entier positif constant, représentant l'état d'une Cellule.
     */
    const unsigned int getC_Etat()const;

    //////////////////////////////////////////////////////

    /**
    *@fn const unsigned int getCouleur() const
    *@brief Permet d'obtenir la couleur d'une Cellule
    *@return Retourne un entier positif constant représentant la valeur d'une couleur.
    */
    const unsigned int getCouleur() const;

    //////////////////////////////////////////////////////

    /**
     *@fn void ViderCellule()
     *@brief La procédure appelle ChangementÉtat(0) pour vider la Cellule. Et permet de réinitialiser une partie par la suite.
     */
    void ViderCellule();

    //////////////////////////////////////////////////////

    /**
     * @fn void ChangementÉtat(const unsigned int changement)
     * @brief Procédure permettant de changer l'état d'une Cellule.
     * Elle peut modifier l'état et les quantities des phéromones d'une Cellule. <br><br>
     * Case 0 : C_Etat devient VIDE de type Etat (enum). pheromone_maison et pheromone_nourriture sont à 0. <br><br>
     * Case 1 : SI C_Etat vaut VIDE <br> ALORS C_Etat devient NOURRITURE de type Etat (enum). pheromone_maison et pheromone_nourriture sont à 0. <br><br>
     * Case 2 : SI C_Etat different de COLONIE <br> ALORS C_Etat devient MUR. pheromone_maison et pheromone_nourriture sont à 0. <br><br>
     * Case 3 : SI C_Etat vaut VIDE <br> ALORS C_Etat devient COLONIE. pheromone_maison prend +1. <br><br>
     * Case 4 : SI C_Etat vaut VIDE ou C_Etat vaut COLONIE <br>ALORS : SI pheromone_maison vaut 0 ALORS elle prend la valeur 100. SI pheromone_maison < 256 ALORS pheromone_maison prend +8.<br><br>
     * Case 5 : SI C_Etat vaut VIDE ou C_Etat vaut COLONIE <br>ALORS : SI pheromone_maison vaut 0 ALORS elle prend la valeur 100. SI pheromone_maison < 256 ALORS pheromone_maison prend +5.<br><br>
     * Case 6 : à enlever
     * @param[in] changement : un entier positif
     * @fn Procédure permettant de changer l'état d'une Cellule
      */
    void ChangementEtat(const unsigned int changement);

    //////////////////////////////////////////////////////

    /**
     *@fn const unsigned int getPheromone_maison()const
     *@brief Permet d'obtenir la valeur du pheromone_maison.
     *@return Retourne un entier positif constant, représentant la pheromone maison.
     */
    const unsigned int getPheromone_maison()const;

    //////////////////////////////////////////////////////

    /**
     *@fn void setPheromone_maison(const unsigned int etat_pheromone_maison)
     *@brief Permet de changer la valeur de la pheromone_maison.
     *@param[in] etat_pheromone_maison : un entier positif représentant la nouvelle valeur pour la phéromone maison.
     */
    void setPheromone_maison(const unsigned int etat_pheromone_maison);

    //////////////////////////////////////////////////////

    /**
     *@fn const unsigned int getPheromone_nourriture()const
     *@brief Permet d'obtenir la valeur du pheromone_nourriture.
     *@return Retourne un entier positif non modifiable, representant la pheromone nourriture.
     */
    const unsigned int getPheromone_nourriture()const;

    //////////////////////////////////////////////////////

    /**
     *@fn void setPheromone_nourriture(const unsigned int etat_pheromone_nourriture)
     *@brief Permet de changer la valeur de la pheromone_nourriture.
     *@param[in] etat_pheromone_nourriture : un entier positif représentant la nouvelle valeur pour la phéromone nourriture.
     */
    void setPheromone_nourriture(const unsigned int etat_pheromone_nourriture);
    //////////////////////////////////////////////////////

    /**
     * @fn const unsigned int EtatCellule() const
     * @brief Permet d'avoir l'état d'une cellule.
     * @return retourne un entier positif constant, représentant un état.
     */
    const unsigned int EtatCellule() const;

    //////////////////////////////////////////////////////

    /**
     **@fn void setPositionCellule(const Vecteur &position)
     * @brief Permet de positionner une cellule à la position donnée en paramètre.
     * @param[in] position : un Vecteur représentant les nouvelles coordonnées de la Cellule
     */
    void setPositionCellule(const Vecteur &position);

    //////////////////////////////////////////////////////

    /**
     * @fn const Vecteur &getPositionCellule() const
     * @brief Permet d'obtenir la position d'une Cellule.
     * @return Retourne un Vecteur constant représentant la position d'une Cellule.
     */
    const Vecteur &getPositionCellule() const;

    //////////////////////////////////////////////////////
};

#endif