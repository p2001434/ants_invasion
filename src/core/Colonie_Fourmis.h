/**
 * @brief Module définissant une Colonie
 * @file Colonie_Fourmis.h
 * @date 2023/03/05
 */

#ifndef _COLONIE_FOURMIS_H
#define _COLONIE_FOURMIS_H

#include "Vecteur.h"
#include "Fourmi.h"
#include <vector>

using namespace std;

/**
 * @brief Colonie de fourmis. Une Colonie est représentée par un Vecteur position (x,y) et un vector vers des Fourmi.
 * 
 */
class Colonie
{

private:
  /**
   * @brief position_Colonies est un Vecteur(x,y).
   */
  Vecteur position_Colonie;
  
  /**
   * @brief nombre_fourmis : un entier positif, représentant le nombre de fourmis total.
   */
  unsigned int nombre_fourmis;

public:
  /**
   * @brief tabFourmi : un vector (tableau dynamique) de fourmis.
   */
  vector<Fourmi> tabFourmi;

  /**
   * @brief compteur_nourriture : un entier positif représentant le nombre de nourriture stocké.
   */
  unsigned int compteur_nourriture;

  //////////////////////////////////////////////////////
  /**
   * @brief Constructeur d'une colonie. Une simple Colonie inutilisable.
   */
  Colonie();

  //////////////////////////////////////////////////////

  /**
   * @fn Colonie(const Vecteur &point_apparition, const unsigned int nombre_fourmi).
   * @brief initialise une Colonie à une position_apparition et le nombre de Fourmi.
   * @param[in] point_apparition : un Vecteur constant
   * @param[in] nombre_fourmi : entier positif
   */
  Colonie(const Vecteur &point_apparition, const unsigned int nombre_fourmi);

  //////////////////////////////////////////////////////

  /**
   * @fn  unsigned int getNombreFourmis()
   * @brief permet de récupérer le nombre de Fourmi.
   * @return un entier positif
   */
  const unsigned int getNombreFourmis()const;

  //////////////////////////////////////////////////////

  /**
   * @fn void setNombreFourmis(const unsigned int nv_nb_fourmis)
   * @brief permet de changer le nombre de Fourmi.
   * @param[in] nv_nb_fourmis : un entier positif constant représentant le nouveau nombre de Fourmis dans la Colonie.
   */
  void setNombreFourmis(const unsigned int nv_nb_fourmis);

  //////////////////////////////////////////////////////

  /**
   * @fn void ajouteFourmi()
   * @brief ajoute une Fourmi à la Colonie en ne dépassant pas le nombre max de Fourmi.
   */
  void ajouteFourmi();

  //////////////////////////////////////////////////////

  /**
   * @fn void enleverFourmi(const unsigned int numero_fourmi)
   * @brief Permet d'enlever une Fourmi en ayant son indice passé en paramètre.
   * @param[in] numero_fourmi : un entier positif constant 
   */
  void enleverFourmi(const unsigned int numero_fourmi);

  //////////////////////////////////////////////////////

  /**
   * @fn const Vecteur getPosition() const
   * @brief Permet d'obtenir le Vecteur position de la Colonie.
   * @return retourne un Vecteur représentant la position de la Colonie.
   */
  const Vecteur getPosition() const;

  //////////////////////////////////////////////////////
};
#endif
