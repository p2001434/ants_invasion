/**
 * @file Cellule.cpp
 * @brief Définition des fonctions et procédures de Fourmi.h
 * @date 2023-04-30
 */
#include "Cellule.h"
#include "Vecteur.h"
#include <random>

Cellule::Cellule()
{
    C_Etat = VIDE;
    pheromone_maison = VIDE;
    pheromone_nourriture = VIDE;
    position_Cellule.x = VIDE;
    position_Cellule.y = VIDE;
    couleur = rand() % 5;
}

const unsigned int Cellule::getC_Etat() const
{
    return C_Etat;
}

void Cellule::ViderCellule()
{
    ChangementEtat(VIDE);
}

void Cellule::ChangementEtat(const unsigned int changement)
{

    switch ((int)changement)
    {

    case 0:
        C_Etat = VIDE;
        pheromone_maison = 0;
        pheromone_nourriture = 0;
        break;
    case 1:
        if (C_Etat == VIDE)
        {
            C_Etat = NOURRITURE;
            pheromone_maison = 0;
            pheromone_nourriture = 0;
        }
        break;
    case 2:
        if (!(C_Etat == COLONIE))
        {

            C_Etat = MUR;
            pheromone_maison = 0;
            pheromone_nourriture = 0;
        }
        break;
    case 3:
        if (C_Etat == VIDE)
        {
            C_Etat = COLONIE;
            pheromone_maison = 255;
        }
        break;
    case 4:
        if ((C_Etat == VIDE) || (C_Etat == COLONIE))
        {
            if (pheromone_maison == 0)
            {
                pheromone_maison = Cellule::INIT_PHE_MAISON;
            }
            if (pheromone_maison < 256)
            {
                pheromone_maison = pheromone_maison + Cellule::AJOUT_PHE_MAISON;
            }
        }
        break;
    case 5:
        if ((C_Etat == VIDE) || (C_Etat == COLONIE))
        {
            if (pheromone_nourriture == 0)
            {
                pheromone_nourriture = Cellule::INIT_PHE_NOURRITURE;
            }
            if (pheromone_nourriture < 256)
            {
                pheromone_nourriture = pheromone_nourriture + Cellule::AJOUT_PHE_NOURRITURE;
            }
        }
        break;
    case 6:
        if ((C_Etat == VIDE) && (pheromone_nourriture))
        {
            pheromone_nourriture = 0;
        }
        break;
    default:
        ViderCellule();
    }
}

const unsigned int Cellule::getPheromone_maison() const
{
    return pheromone_maison;
}

void Cellule::setPheromone_maison(const unsigned int etat_pheromone_maison)
{
    pheromone_maison = etat_pheromone_maison;
}

const unsigned int Cellule::getPheromone_nourriture() const
{
    return pheromone_nourriture;
}

void Cellule::setPheromone_nourriture(const unsigned int etat_pheromone_nourriture)
{
    pheromone_nourriture = etat_pheromone_nourriture;
}

const unsigned int Cellule::EtatCellule() const
{
    return C_Etat;
}

void Cellule::setPositionCellule(const Vecteur &position)
{
    position_Cellule = position;
}

const Vecteur &Cellule::getPositionCellule() const
{
    return position_Cellule;
}

const unsigned int Cellule::getCouleur() const
{
    return couleur;
}