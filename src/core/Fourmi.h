/**
 * @brief Module définissant une Fourmi
 * @file Fourmi.h
 * @date 2023/03/05
 */

#ifndef _FOURMI_H
#define _FOURMI_H

#include "Vecteur.h"

/**
 * @brief une Fourmi est représenté par un Vecteur position (x,y) et d'un booléen p_nourriture (porte-t-elle de la nourriture)
 */
class Fourmi
{
private:
	/**
	 * @brief La position(x,y) de la fourmi.
	 */
	Vecteur position;

public:
	/**
	 *@brief Booléen pour savoir si la Fourmi porte de la Nourriture
	 */
	bool p_nourriture;

	//////////////////////////////////////////////////////

	/**
	 * @fn Fourmi(const Vecteur &position_colonie)
	 * @brief Constructeur de la classe Fourmi. Initialise une Fourmi à proximité de la Colonie.
	 * @param[in] position_colonie : un Vecteur constant 
	 */
	Fourmi(const Vecteur &position_colonie);

	//////////////////////////////////////////////////////

	/**
	 * @fn const Vecteur &getPosition() const
	 * @brief Récupère les coordonnés(x,y) de la Fourmi.
	 * @return : Un Vecteur constant
	 */
	const Vecteur &getPosition() const;

	//////////////////////////////////////////////////////

	/**
	 * @fn void setPosition(const Vecteur &nouveau_vecteur)
	 * @brief Mutateur : modifie le Vecteur de la Fourmi par un nouveau Vecteur
	 * @param[in] nouveau_vecteur : un Vecteur constant
	 */
	void setPosition(const Vecteur &nouveau_vecteur);

	//////////////////////////////////////////////////////
};

#endif