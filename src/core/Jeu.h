/**
 * @brief Module définissant un Jeu
 * @file Jeu.h
 * @date 2023/03/06
 */

#ifndef _JEU_H
#define _JEU_H

#include "Terrain.h"

/**
 * @brief un Jeu est composé d'un Terrain
 */
class Jeu
{
private:
public:
  
  /**
   * @brief Terrain sur lequel va se dérouler la Partie de Jeu
   * 
   */
  Terrain Jeu_Terrain;

  //////////////////////////////////////////////////////

  /**
   * @fn Jeu(const unsigned int DIMX, const unsigned int DIMY)
   * @brief Constructeur de Terrain. Crée un Terrain de dimension DIMX * DIMY.
   * @param[in] DIMX : entier positif constant, taille X du Terrain de Jeu.
   * @param[in] DIMY : entier positif constant, taille Y du Terrain de Jeu.
   */
  Jeu(const unsigned int DIMX, const unsigned int DIMY);

  //////////////////////////////////////////////////////


  /**
   * @fn void TerrainSecret()
   * @brief Procédure qui initialise des murs sur un Terrain.
   */
  void TerrainSecret();

  //////////////////////////////////////////////////////

  /**
   * @fn void RedemarrerPartie()
   * @brief Réinitialise un Terrain, le prepare et commence la simulation
   * Procédure permettant de Redémarrer la Partie en réinitialisant tout le Terrain 
   */
  void RedemarrerPartie();

  //////////////////////////////////////////////////////

  /**
   * @fn void ActualisationPartie()
   * @brief Calcule les modifications de Terrain et les applique
   * @return
   */
  void ActualisationPartie();

  //////////////////////////////////////////////////////


  /**
   * @fn void TestDeRegression()
   * @brief teste de régression qui va tester toutes les fonctions et procédures du core
   */
  void TestDeRegression();

  //////////////////////////////////////////////////////

  /**
   * @fn void AffichageTerrainSimpleTest()
   * @brief un affichage du terrain sans les Fourmi
   */
  void AffichageTerrainSimpleTest()const;

  //////////////////////////////////////////////////////

  /**
   * @fn void AffichageTerrainTest()
   * @brief affiche pour le test de regression le terrain
   */
  void AffichageTerrainTest()const;

  //////////////////////////////////////////////////////
};

#endif