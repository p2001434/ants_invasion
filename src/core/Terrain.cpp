/**
 * @file Terrain.cpp
 * @brief Définition des fonctions et procédures de Terrain.h
 * @date 2023-04-30
 */

#include "Terrain.h"
#include "Cellule.h"
#include <random>
#include <iostream>
#include <algorithm>

Terrain::Terrain(const unsigned int dimX, const unsigned int dimY)
{
    Vecteur positionCellule;
    if ((dimX < 0) || (dimY < 0) || (dimX > 500) || (dimY > 500))
    {
        DIMX = 100;
        DIMY = 100;
    }
    else
    {
        DIMX = dimX; // la hauteur
        DIMY = dimY; // la largeur
    }
    tab_Terrain_Cellule.resize(DIMX * DIMY);

    for (unsigned int i = 0; i < DIMX; i++)
    {

        positionCellule.x = i;
        for (unsigned int j = 0; j < DIMY; j++)
        {
            positionCellule.y = j;
            tab_Terrain_Cellule[i * DIMY + j].setPositionCellule(positionCellule);
        }
    }
    Vecteur positionAleatoire;
    positionAleatoire.x = rand() % (DIMX / 2) + (DIMX / 2) / 2;
    positionAleatoire.y = rand() % (DIMY / 2) + (DIMY / 2) / 2;

    unsigned int nombre_f = 15;
    Fourmiliere = Colonie(positionAleatoire, nombre_f);
    for (unsigned int i = 0; i < DIMX; i++)
    {
        for (unsigned int j = 0; j < DIMY; j++)
        {
            if (i * DIMY + j == (Fourmiliere.getPosition().x) * DIMY + (Fourmiliere.getPosition().y))
            {
                for (unsigned int b = i - 1; b <= i + 1; b++)
                {
                    for (unsigned int c = j - 1; c <= j + 1; c++)
                    {
                        tab_Terrain_Cellule[b * DIMY + c].ChangementEtat(Cellule::COLONIE);
                    }
                }
            }
            if ((i == 0) || (j == 0) || (j == DIMY - 1) || (i == DIMX - 1))
            {
                //unsigned int DEUX = 2;
                tab_Terrain_Cellule[i * (unsigned int)DIMY + j].ChangementEtat(Cellule::MUR);
            }
        }
    }
    int v;
    int u;
    bool ni_nourriture_ni_mur = false;
    do
    {
        v = rand() % (DIMX - 5) + 3;
        u = rand() % (DIMY - 5) + 3;
        for (int m = v - 1; m <= v + 1; m++)
        {
            for (int n = u - 1; n <= u + 1; n++)
            {
                if (tab_Terrain_Cellule[m * DIMY + n].EtatCellule() != Cellule::VIDE)
                {
                    ni_nourriture_ni_mur = true;
                }
            }
        }
    } while (ni_nourriture_ni_mur);
    for (int m = v - 1; m <= v + 1; m++)
    {
        for (int n = u - 1; n <= u + 1; n++)
        {
            tab_Terrain_Cellule[m * DIMY + n].ChangementEtat(Cellule::NOURRITURE);
        }
    }
}


const unsigned int Terrain::getEtat(const unsigned int x,const unsigned int y)const
{
    return tab_Terrain_Cellule[x * DIMY + y].getC_Etat();
}

void Terrain::AjouteMur(const Vecteur &position_souris)
{

    if ((position_souris.x > 0) && (position_souris.y > 0))
    {

        if ((position_souris.x < DIMX) && (position_souris.y < DIMY))
        {

            if (tab_Terrain_Cellule[position_souris.y + DIMY * position_souris.x].EtatCellule() != Cellule::COLONIE)
            {
                int i;
                tab_Terrain_Cellule[position_souris.y + DIMY * position_souris.x].ChangementEtat(Cellule::MUR);
                for (i = Fourmiliere.tabFourmi.size() - 1; i >= 0; i--)
                {
                    if (Fourmiliere.tabFourmi[i].getPosition() == position_souris)
                    {
                        Fourmiliere.enleverFourmi((unsigned int)i);
                        Fourmiliere.setNombreFourmis(Fourmiliere.tabFourmi.size());
                    }
                }
            }
        }
    }
}


void Terrain::AjouteNourriture(const Vecteur &position_souris)
{
    if ((position_souris.x > 0) && (position_souris.y > 0) && (position_souris.x < DIMX) && (position_souris.y < DIMY))
    {
        bool f_présente = false;
        for (unsigned int i = 0; i < Fourmiliere.tabFourmi.size(); i++)
        {
            if (Fourmiliere.tabFourmi[i].getPosition() == position_souris)
            {
                f_présente = true;
            }
        }
        if (!f_présente)
        {
            tab_Terrain_Cellule[position_souris.x * DIMY + position_souris.y].ChangementEtat(Cellule::NOURRITURE);
        }
    }
}

void Terrain::NaissanceFourmi()
{
    if ((Fourmiliere.compteur_nourriture > MAX_NOURRITURE) && (Fourmiliere.tabFourmi.size() < NB_MAX_FOURMI))
    {
        Fourmiliere.ajouteFourmi();
        Fourmiliere.setNombreFourmis(Fourmiliere.getNombreFourmis() + 1);
        Fourmiliere.compteur_nourriture = Fourmiliere.compteur_nourriture - 10;
    }
}

void Terrain::LacherPheromone(const unsigned int indice_fourmi)
{
    if (!(Fourmiliere.tabFourmi[indice_fourmi].p_nourriture))
    {
        tab_Terrain_Cellule[Fourmiliere.tabFourmi[indice_fourmi].getPosition().y + DIMY * Fourmiliere.tabFourmi[indice_fourmi].getPosition().x].ChangementEtat(4);
    }
    else
    {
        tab_Terrain_Cellule[Fourmiliere.tabFourmi[indice_fourmi].getPosition().y + DIMY * Fourmiliere.tabFourmi[indice_fourmi].getPosition().x].ChangementEtat(5);
    }
}

void Terrain::NettoyagePheromone()
{
    for (unsigned int i = 0; i < DIMX; i++)
    {
        for (unsigned int j = 0; j < DIMY; j++)
        {
            if (tab_Terrain_Cellule[j + DIMY * i].getPheromone_maison() != 0)
            {
                tab_Terrain_Cellule[j + DIMY * i].setPheromone_maison(tab_Terrain_Cellule[j + DIMY * i].getPheromone_maison() - 1);
            }
            if (tab_Terrain_Cellule[j + DIMY * i].getPheromone_nourriture() != 0)
            {
                tab_Terrain_Cellule[j + DIMY * i].setPheromone_nourriture(tab_Terrain_Cellule[j + DIMY * i].getPheromone_nourriture() - 1);
            }
        }
    }
}

void Terrain::DeplacementFourmi(unsigned int numero_de_fourmi)
{
    vector<Cellule> PositionPossible;
    Cellule tampon;
    Vecteur tampon_deux;
    Vecteur vn = Fourmiliere.tabFourmi[numero_de_fourmi].getPosition();
    for (unsigned int i = vn.x - 1; i <= vn.x + 1; i++)
    {
        for (unsigned int j = vn.y - 1; j <= vn.y + 1; j++)
        {
            if (!((i == vn.x) && (j == vn.y)) && (tab_Terrain_Cellule[j + DIMY * i].getC_Etat() != Cellule::MUR) && (tab_Terrain_Cellule[j + DIMY * i].getC_Etat() != Cellule::NOURRITURE))
            {
                tampon_deux.x = i;
                tampon_deux.y = j;
                tampon.setPositionCellule(tampon_deux);
                PositionPossible.push_back(tampon);
            }
        }
    }
    int direction = PositionPossible.size();
    if (direction != 0)
    {

        direction = rand() % direction;
        LacherPheromone(numero_de_fourmi);
        Fourmiliere.tabFourmi[numero_de_fourmi].setPosition(PositionPossible[direction].getPositionCellule());
    }
}

void Terrain::DeplacementAleatoire(const vector<Cellule> &case_a_trier, const unsigned int indice_fourmi)
{
    unsigned int taille = case_a_trier.size();
    Cellule CaseMax;
    CaseMax = case_a_trier[0];
    for (unsigned int i = 0; i < taille; i++)
    {
        if (Fourmiliere.tabFourmi[indice_fourmi].p_nourriture)
        {
            if (CaseMax.getPheromone_maison() > case_a_trier[i].getPheromone_maison())
            {
                if(rand()%100 < 80){
                CaseMax = case_a_trier[i];
                }
            }
            else if (CaseMax.getPheromone_maison() == case_a_trier[i].getPheromone_maison())
            {
                if(rand()%2 == 0){
                    CaseMax=case_a_trier[i];
                }
            }
        }
        else
        {
            if (CaseMax.getPheromone_nourriture() > case_a_trier[i].getPheromone_nourriture())
            {
                if(rand()%100 < 80)
                CaseMax = case_a_trier[i];
                
            }
            else if (CaseMax.getPheromone_nourriture() == case_a_trier[i].getPheromone_nourriture())
            {
                if(rand()%2 == 0){
                    CaseMax=case_a_trier[i];
                }
            }
        }

    }
    LacherPheromone(indice_fourmi);
    Fourmiliere.tabFourmi[indice_fourmi].setPosition(CaseMax.getPositionCellule());
    }

void Terrain::SuivreChemin(const unsigned int IndiceFourmi)
{
    Vecteur pos_fourmi;
    pos_fourmi = Fourmiliere.tabFourmi[IndiceFourmi].getPosition();
    bool porteNourriture = Fourmiliere.tabFourmi[IndiceFourmi].p_nourriture;
    bool changementPosition = false;
    unsigned int i, j;
    bool trouver_Pheromone = false;
    vector<Cellule> tableau_cellule_pheromone;
    for (i = pos_fourmi.x - 1; i <= pos_fourmi.x + 1; i++)
    {
        for (j = pos_fourmi.y - 1; j <= pos_fourmi.y + 1; j++)
        {

            if (((!changementPosition) && (!((pos_fourmi.x == i) && (pos_fourmi.y == j)))))
            {
                if (!porteNourriture)
                {
                    if (tab_Terrain_Cellule[j + (DIMY * i)].getPheromone_nourriture() != 0)
                    {
                        
                        tableau_cellule_pheromone.push_back(tab_Terrain_Cellule[j + (DIMY * i)]);
                        trouver_Pheromone = true;
                    }
                    else
                    {
                        if ((!trouver_Pheromone) && (i == pos_fourmi.x + 1) && (j == pos_fourmi.y + 1))
                        {
                            DeplacementFourmi(IndiceFourmi);
                        }
                    }
                }
                else
                {
                    if (tab_Terrain_Cellule[j + DIMY * i].getPheromone_maison() != 0)
                    {
                        tableau_cellule_pheromone.push_back(tab_Terrain_Cellule[j + (DIMY * i)]);
                        trouver_Pheromone = true;
                    }
                    else if ((!trouver_Pheromone) && (i == pos_fourmi.x + 1) && (j == pos_fourmi.y))
                    {
                        DeplacementFourmi(IndiceFourmi);
                    }
                }
            }
        }
    }
    if (trouver_Pheromone)
    {
        DeplacementAleatoire(tableau_cellule_pheromone, IndiceFourmi);
    }
}




const unsigned int Terrain::AttrapeNourriture(const unsigned int IndiceFourmi)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    Vecteur pos_fourmi;
    pos_fourmi = Fourmiliere.tabFourmi[IndiceFourmi].getPosition();

    // boucle qui parcours autour de la fourmi, pas très fonctionnel
    unsigned int xx, yy;
    bool action = false;
    Cellule une_cellule;
    vector<Cellule> Tableau_Cellule;

    for (xx = pos_fourmi.x - 1; xx <= pos_fourmi.x + 1; xx++)
    {
        for (yy = pos_fourmi.y - 1; yy <= pos_fourmi.y + 1; yy++)
        {
            une_cellule = tab_Terrain_Cellule[xx * DIMY + yy];
            Tableau_Cellule.push_back(une_cellule);
        }
    }

    std::shuffle(Tableau_Cellule.begin(), Tableau_Cellule.end(), gen); // va mélanger le tableau de manière aléatoire

    for (unsigned int i = 0; i < Tableau_Cellule.size(); i++)
    {
        if (!action)
        {

            if (!(Tableau_Cellule[i].getPositionCellule() == pos_fourmi))
            {
                xx = Tableau_Cellule[i].getPositionCellule().x;
                yy = Tableau_Cellule[i].getPositionCellule().y;
                if (!(Fourmiliere.tabFourmi[IndiceFourmi].p_nourriture)) // si elle porte pas de nourriture
                {
                    if (Tableau_Cellule[i].getC_Etat() == Cellule::NOURRITURE) // et qu'une case de nourriture est a proximité
                    {
                        LacherPheromone(IndiceFourmi);
                        tab_Terrain_Cellule[xx * DIMY + yy].ChangementEtat(Cellule::VIDE);
                        Fourmiliere.tabFourmi[IndiceFourmi].p_nourriture = true;
                        action = true;
                        LacherPheromone(IndiceFourmi);
                    }
                }
                else if (Tableau_Cellule[i].getC_Etat() == Cellule::COLONIE) // si elle porte de la nourriture et qu'une case a proximité fait partie de la colonie
                {
                    Fourmiliere.tabFourmi[IndiceFourmi].p_nourriture = false;
                    Fourmiliere.compteur_nourriture++;
                    action = true;
                }
            }
        }
    }

    if (action)
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

void Terrain::MouvementDesFourmis()
{
    unsigned int a = Fourmiliere.tabFourmi.size();
    if (a != 0)
    {
        for (unsigned int i = 0; i < a; i++)
        {
            if (AttrapeNourriture(i) == 0)
            {
                SuivreChemin(i);
            }
        }
    }
}