/**
 * @file Jeu.cpp
 * @brief Définition des fonctions et procédures de Jeu.h
 * @date 2023-04-30
 */

#include "Jeu.h"
#include <iostream>
#include <cassert>
#include <time.h>
#include <unistd.h>

Jeu::Jeu(const unsigned int DIMX, const unsigned int DIMY) : Jeu_Terrain(DIMX, DIMY) {}

void Jeu::TerrainSecret()
{

     Vecteur alpha(3, 3);
     Colonie test(alpha, 3);
     Jeu_Terrain.Fourmiliere = test;
     Jeu_Terrain.DIMX = 20;
     Jeu_Terrain.DIMY = 20;
     Jeu_Terrain.tab_Terrain_Cellule.resize(Jeu_Terrain.DIMX * Jeu_Terrain.DIMY);
     for (unsigned int i = 0; i < Jeu_Terrain.DIMX; i++)
     {
          alpha.x = i;
          for (unsigned int j = 0; j < Jeu_Terrain.DIMY; j++)
          {
               alpha.y = j;
               Jeu_Terrain.tab_Terrain_Cellule[i * Jeu_Terrain.DIMY + j].setPositionCellule(alpha);
               if ((i == 0) || (i == Jeu_Terrain.DIMX - 1) || (j == 0) || (j == Jeu_Terrain.DIMY - 1) || ((i == 7) || (i == 13) || (j == 6) || (j == 10) || (j == 14)))
               {

                    Jeu_Terrain.tab_Terrain_Cellule[i * Jeu_Terrain.DIMY + j].ChangementEtat(2);
               }
          }
     }
     alpha = Jeu_Terrain.Fourmiliere.getPosition();
     for (unsigned int i = alpha.x - 1; i <= alpha.x + 1; i++)
     {
          for (unsigned int j = alpha.y - 1; j <= alpha.y + 1; j++)
          {
               Jeu_Terrain.tab_Terrain_Cellule[i * Jeu_Terrain.DIMY + j].ChangementEtat(3);
          }
     }
     alpha.x = 3;
     alpha.y = 17;
     for (unsigned int i = alpha.x - 1; i <= alpha.x + 1; i++)
     {
          for (unsigned int j = alpha.y - 1; j <= alpha.y + 1; j++)
          {
               Jeu_Terrain.tab_Terrain_Cellule[i * Jeu_Terrain.DIMY + j].ChangementEtat(1);
          }
     }
     Jeu_Terrain.tab_Terrain_Cellule[7 * Jeu_Terrain.DIMY + 3].ChangementEtat(0);
     Jeu_Terrain.tab_Terrain_Cellule[7 * Jeu_Terrain.DIMY + 4].ChangementEtat(0);
     Jeu_Terrain.tab_Terrain_Cellule[7 * Jeu_Terrain.DIMY + 8].ChangementEtat(0);

     Jeu_Terrain.tab_Terrain_Cellule[13 * Jeu_Terrain.DIMY + 3].ChangementEtat(0);
     Jeu_Terrain.tab_Terrain_Cellule[13 * Jeu_Terrain.DIMY + 4].ChangementEtat(0);
     Jeu_Terrain.tab_Terrain_Cellule[13 * Jeu_Terrain.DIMY + 8].ChangementEtat(0);
     Jeu_Terrain.tab_Terrain_Cellule[13 * Jeu_Terrain.DIMY + 17].ChangementEtat(0);
     Jeu_Terrain.tab_Terrain_Cellule[13 * Jeu_Terrain.DIMY + 18].ChangementEtat(0);
     Jeu_Terrain.tab_Terrain_Cellule[13 * Jeu_Terrain.DIMY + 12].ChangementEtat(0);

     Jeu_Terrain.tab_Terrain_Cellule[3 * Jeu_Terrain.DIMY + 10].ChangementEtat(0);
     Jeu_Terrain.tab_Terrain_Cellule[2 * Jeu_Terrain.DIMY + 10].ChangementEtat(0);
     Jeu_Terrain.tab_Terrain_Cellule[2 * Jeu_Terrain.DIMY + 14].ChangementEtat(0);
     Jeu_Terrain.tab_Terrain_Cellule[3 * Jeu_Terrain.DIMY + 14].ChangementEtat(0);

     Jeu_Terrain.tab_Terrain_Cellule[16 * Jeu_Terrain.DIMY + 6].ChangementEtat(0);
     Jeu_Terrain.tab_Terrain_Cellule[17 * Jeu_Terrain.DIMY + 6].ChangementEtat(0);
     Jeu_Terrain.tab_Terrain_Cellule[16 * Jeu_Terrain.DIMY + 10].ChangementEtat(0);
     Jeu_Terrain.tab_Terrain_Cellule[17 * Jeu_Terrain.DIMY + 10].ChangementEtat(0);
     Jeu_Terrain.tab_Terrain_Cellule[16 * Jeu_Terrain.DIMY + 14].ChangementEtat(0);
     Jeu_Terrain.tab_Terrain_Cellule[17 * Jeu_Terrain.DIMY + 14].ChangementEtat(0);

     Jeu_Terrain.tab_Terrain_Cellule[10 * Jeu_Terrain.DIMY + 14].ChangementEtat(0);
     Jeu_Terrain.tab_Terrain_Cellule[11 * Jeu_Terrain.DIMY + 14].ChangementEtat(0);
}

void Jeu::RedemarrerPartie(){
     Terrain nouveau(Jeu_Terrain.DIMX,Jeu_Terrain.DIMY);
     Jeu_Terrain=nouveau;
}

void Jeu::ActualisationPartie()
{
     Jeu_Terrain.NettoyagePheromone();
     Jeu_Terrain.MouvementDesFourmis();
}

void Jeu::AffichageTerrainSimpleTest()const
{
     cout << "================================" << '\n'
          << '\n';
     cout << "Representation du Terrain (sans les fourmis) :" << '\n'
          << '\n';
     cout << '\n'
          << '\n'
          << "XX ";

     for (unsigned int TESTO = 0; TESTO < Jeu_Terrain.DIMY; TESTO++)
     {
          if (TESTO < 10)
          {
               cout << "0";
          }
          cout << TESTO << " ";
     }
     cout << '\n';

     for (unsigned int TEST = 0; TEST < Jeu_Terrain.DIMX; TEST++)
     {
          if (TEST < 10)
          {
               cout << "0";
          }
          cout << TEST << "  ";

          for (unsigned int TESTO = 0; TESTO < Jeu_Terrain.DIMY; TESTO++)
          {
               unsigned int TESTLA = Jeu_Terrain.tab_Terrain_Cellule[TEST * Jeu_Terrain.DIMY + TESTO].getC_Etat();
               if (TESTLA == 0)
               {
                    cout << ".  ";
               }
               else if (TESTLA == 2)
               {
                    cout << "M  ";
               }
               else if (TESTLA == 1)
               {
                    cout << "N  ";
               }
               else if (TESTLA == 3)
               {
                    cout << "C  ";
               }
          }
          cout << '\n';
     }
     cout << '\n'
          << "fin d'affichage du terrain" << '\n'
          << '\n';
}

void Jeu::AffichageTerrainTest()const
{
     int compteur_fourmi;
     vector<Fourmi> Tableau_fourmiRestante;
     Vecteur un_vecteur;
     cout << '\n';
     cout << "================================" << '\n'
          << '\n';
     cout << "Representation du Terrain (avec les fourmis) :" << '\n'
          << '\n';
     cout << '\n'
          << '\n'
          << "XX ";

     for (unsigned int TESTO = 0; TESTO < Jeu_Terrain.DIMY; TESTO++)
     {
          if (TESTO < 10)
          {
               cout << "0";
          }
          cout << TESTO << " ";
     }
     cout << '\n';
     compteur_fourmi = 0;
     Tableau_fourmiRestante = Jeu_Terrain.Fourmiliere.tabFourmi;
     for (unsigned int TEST = 0; TEST < Jeu_Terrain.DIMX; TEST++)
     {
          if (TEST < 10)
          {
               cout << "0";
          }
          cout << TEST << "  ";
          un_vecteur.x = TEST;

          for (unsigned int TESTO = 0; TESTO < Jeu_Terrain.DIMY; TESTO++)
          {
               compteur_fourmi = 0;
               un_vecteur.y = TESTO;
               unsigned int TESTLA = Jeu_Terrain.tab_Terrain_Cellule[TEST * Jeu_Terrain.DIMY + TESTO].getC_Etat();

               for (int compte_nb_fourmi = 0; compte_nb_fourmi < (int)Tableau_fourmiRestante.size(); compte_nb_fourmi++)
               {
                    if (Tableau_fourmiRestante[compte_nb_fourmi].getPosition() == un_vecteur)
                    {
                         compteur_fourmi++;
                         Tableau_fourmiRestante.erase(Tableau_fourmiRestante.begin() + compte_nb_fourmi);
                         compte_nb_fourmi = -1;
                    }
               }
               if (compteur_fourmi != 0)
               {
                    if (compteur_fourmi == 1)
                    {
                         cout << "F  ";
                    }
                    else
                    {
                         if (compteur_fourmi < 10)
                         {
                              cout << compteur_fourmi << "  ";
                         }
                         else
                         {
                              cout << compteur_fourmi << " ";
                         }
                    }
               }
               else
               {
                    if (TESTLA == 0)
                    {
                         cout << ".  ";
                    }
                    else if (TESTLA == 2)
                    {
                         cout << "M  ";
                    }
                    else if (TESTLA == 1)
                    {
                         cout << "N  ";
                    }
                    else if (TESTLA == 3)
                    {
                         cout << "C  ";
                    }
               }
          }
          cout << '\n';
     }
     cout << '\n'
          << "La colonie possède " << Jeu_Terrain.Fourmiliere.compteur_nourriture << " de nourriture (il en faut + de 10 pour crée une nouvelle fourmi)"
          << '\n'
          << '\n'
          << "fin d'affichage du terrain" << '\n'
          << '\n';
}

void Jeu::TestDeRegression()
{

     cout << "Debut du Test de Regression : ";

     // Vecteur
     cout << '\n'
          << '\n'
          << "creation d'un vecteur"
          << '\n'
          << '\n';
     Vecteur un_vecteur;
     cout << "position x du vecteur à t0 : " << un_vecteur.x
          << '\n'
          << '\n';
     un_vecteur.x = -10;
     cout << "position x du vecteur à t1 quand on lui assigne x = -10 :" << un_vecteur.x << '\n';
     un_vecteur.y = 5;
     un_vecteur.x = 10;

     // Fourmi
     cout << '\n'
          << '\n'
          << "creation d'une fourmis par le vecteur x: 5 et y: 10"
          << '\n'
          << '\n';
     Fourmi une_fourmi(un_vecteur);
     cout << "position x de la fourmis : " << une_fourmi.getPosition().x
          << '\n'
          << '\n';
     cout << "position y de la fourmis : " << une_fourmi.getPosition().y
          << '\n'
          << '\n';
     cout << "Changement de la position de la fourmis par x:10 et y:11 "
          << '\n'
          << '\n';
     un_vecteur.x = 10;
     un_vecteur.y = 11;
     une_fourmi.setPosition(un_vecteur);
     cout << "nouvelle position x de la fourmis : " << une_fourmi.getPosition().x
          << '\n'
          << '\n';
     cout << "nouvelle position y de la fourmis : " << une_fourmi.getPosition().y
          << '\n'
          << '\n';
     sleep(3);
     // Colonie
     cout << "Creation d'une colonie JeuTest avec 10 fourmi aux cordonnées x:10 et y:11 "
          << '\n'
          << '\n';
     Colonie une_colonie(un_vecteur, 10);
     cout << "Position colonie en x : " << une_colonie.getPosition().x << " et en y: " << une_colonie.getPosition().y
          << '\n'
          << '\n'
          << "et a pour nombre de Fourmi : " << une_colonie.getNombreFourmis()
          << '\n'
          << '\n';
     cout << "Ajout d'une Fourmi dans la colonie et repositionnement en x:20 y:20"
          << '\n'
          << '\n';
     une_colonie.ajouteFourmi();
     un_vecteur.x = un_vecteur.y = 20;
     une_colonie.tabFourmi[10].setPosition(un_vecteur);
     for (unsigned int i = 0; i < une_colonie.tabFourmi.size(); i++)
     {
          cout << "Je suis la Fourmi " << i << " et je suis en position x:"
               << une_colonie.tabFourmi[i].getPosition().x << " et y:"
               << une_colonie.tabFourmi[i].getPosition().y
               << '\n'
               << '\n';
     }
     cout << "La colonie de fourmi possède donc maintenant " << une_colonie.getNombreFourmis() << " de Fourmi"
          << '\n'
          << '\n';
     cout << "Test de la destruction de la 11ième Fourmi :"
          << '\n'
          << '\n';
     une_colonie.enleverFourmi(10);
     for (unsigned int i = 0; i < une_colonie.tabFourmi.size(); i++)
     {
          cout << "Je suis la Fourmi " << i << " et je suis en position x:"
               << une_colonie.tabFourmi[i].getPosition().x << " et y:"
               << une_colonie.tabFourmi[i].getPosition().y
               << '\n'
               << '\n';
     }
     sleep(3);
     // Cellule

     cout << "Creation d'une Cellule avec le constructeur vide :"
          << '\n'
          << '\n';
     Cellule une_cellule_de_base;
     cout << "Affichage des Caractéristiques de la Cellule : "
          << '\n'
          << '\n'
          << "Son État à pour valeur : " << une_cellule_de_base.EtatCellule() << " (0 pour vide , 1 pour nourriture , 2 pour mur , 3 pour une colonie)"
          << '\n'
          << '\n'
          << "Sa valeur pour les pheromones est :"
          << '\n'
          << '\n'
          << "maison : " << une_cellule_de_base.getPheromone_maison()
          << '\n'
          << '\n'
          << "nourriture : " << une_cellule_de_base.getPheromone_nourriture()
          << '\n'
          << '\n'
          << "et a pour vecteur x=" << une_cellule_de_base.getPositionCellule().x << " et y=" << une_cellule_de_base.getPositionCellule().y
          << '\n'
          << '\n';

     cout << "Changement de la position de la cellule par x:20 et y:20"
          << '\n'
          << '\n';
     une_cellule_de_base.setPositionCellule(un_vecteur);
     cout << "La cellule est maintenant en position x=" << une_cellule_de_base.getPositionCellule().x << " et y=" << une_cellule_de_base.getPositionCellule().y
          << '\n'
          << '\n';
     cout << "Test Changement d'État progressif :"
          << '\n'
          << '\n';
     for (unsigned int i = 0; i < 8; i++)
     {
          cout << "========================"
               << '\n'
               << '\n';
          cout << "Test ChangementEtat(" << i << ");"
               << '\n';
          une_cellule_de_base.ChangementEtat(i);
          cout << "Affichage des Caractéristiques de la Cellule : "
               << '\n'
               << '\n'
               << "Son État à pour valeur : " << une_cellule_de_base.EtatCellule() << " (0 pour vide , 1 pour nourriture , 2 pour mur , 3 pour une colonie)"
               << '\n'
               << '\n'
               << "Sa valeur pour les pheromones est :"
               << '\n'
               << '\n'
               << "maison : " << une_cellule_de_base.getPheromone_maison()
               << '\n'
               << '\n'
               << "nourriture : " << une_cellule_de_base.getPheromone_nourriture()
               << '\n'
               << '\n'
               << "et a pour vecteur x=" << une_cellule_de_base.getPositionCellule().x << " et y=" << une_cellule_de_base.getPositionCellule().y
               << '\n'
               << '\n'
               << '\n';
          une_cellule_de_base.ChangementEtat(-12);
     }
     sleep(3);
     une_cellule_de_base.~Cellule();

     // Terrain

     cout << "Le terrain a déjà été fabriqué dans le main, affichage de ses caractéristiques :"
          << '\n'
          << '\n';
     cout << "La dimension X (la hauteur) du terrain est de : " << Jeu_Terrain.DIMX
          << '\n'
          << '\n';
     cout << "La dimension Y (la largeur) du terrain est de : " << Jeu_Terrain.DIMY
          << '\n'
          << '\n';
     cout << "La taille du terrain est théoriquement de : " << Jeu_Terrain.DIMX * Jeu_Terrain.DIMY
          << '\n'
          << '\n';
     cout << "Le Nombre de Cellule du vecteur du terrain est de : " << Jeu_Terrain.tab_Terrain_Cellule.size()
          << '\n'
          << '\n';
     cout << "Le terrain peut être représenté comme ceci :"
          << '\n'
          << '\n';

     if ((0 == Jeu_Terrain.tab_Terrain_Cellule.size()) || (Jeu_Terrain.DIMX < 3) || (Jeu_Terrain.DIMY < 3))
     {
          cout << "Le terrain est vide ou trop petit et ne peut pas être représenté." << '\n'
               << '\n';
     }
     else
     {
          for (unsigned int i = 0; i < Jeu_Terrain.DIMX; i++)
          {
               cout << "ligne ";
               if (i < 10)
               {
                    cout << "0";
               }
               cout << i << " : ";
               for (unsigned int j = 0; j < Jeu_Terrain.DIMY; j++)
               {
                    cout << j << " ";
               }
               cout << '\n';
          }
          cout << '\n'
               << '\n'
               << "fin de representation"
               << '\n'
               << '\n';

          cout << "Chaque cellule possède une coordonné en x et y logique (essaie avec les 3 premieres lignes et la dernière) :"
               << '\n'
               << '\n';
          for (unsigned int i = 0; i < 3; i++)
          {
               for (unsigned int j = 0; j < Jeu_Terrain.DIMY; j++)
               {

                    cout << "la case : ";
                    if ((i * Jeu_Terrain.DIMY) + j < 10)
                    {
                         cout << "0";
                    }
                    cout << i * Jeu_Terrain.DIMY + j;

                    if (i < 10)
                    {
                         cout << " doit être de cordonnée x: 0" << i;
                         if (j < 10)
                         {
                              cout << " et y: 0" << j;
                         }
                         else
                         {
                              cout << " et y: " << j;
                         }
                    }
                    else
                    {

                         cout << "Doit être de cordonnée x:" << i;

                         if (j < 10)
                         {
                              cout << " et y: 0" << j;
                         }
                         else
                         {
                              cout << " et y: " << j;
                         }
                    }
                    cout << "     et est de coordonnée         x: ";
                    if (Jeu_Terrain.tab_Terrain_Cellule[(i * Jeu_Terrain.DIMY) + j].getPositionCellule().x < 10)
                    {
                         cout << "0";
                    }
                    cout << Jeu_Terrain.tab_Terrain_Cellule[(i * Jeu_Terrain.DIMY) + j].getPositionCellule().x
                         << " et y: ";
                    if (Jeu_Terrain.tab_Terrain_Cellule[(i * Jeu_Terrain.DIMY) + j].getPositionCellule().y < 10)
                    {
                         cout << "0";
                    }

                    cout << Jeu_Terrain.tab_Terrain_Cellule[(i * Jeu_Terrain.DIMY) + j].getPositionCellule().y << "  et a comme État: " << Jeu_Terrain.tab_Terrain_Cellule[(i * Jeu_Terrain.DIMY) + j].getC_Etat()
                         << '\n';
               }
               cout << '\n';
          }
          cout << '\n';
          unsigned int i = Jeu_Terrain.DIMX - 1;
          for (unsigned int j = 0; j < Jeu_Terrain.DIMY; j++)
          {

               cout << "la case : ";
               if ((i * Jeu_Terrain.DIMY) + j < 10)
               {
                    cout << "0";
               }
               cout << i * Jeu_Terrain.DIMY + j;

               if (i < 10)
               {
                    cout << " doit être de cordonnée x: 0" << i;
                    if (j < 10)
                    {
                         cout << " et y: 0" << j;
                    }
                    else
                    {
                         cout << " et y: " << j;
                    }
               }
               else
               {

                    cout << " doit être de cordonnée x:" << i;

                    if (j < 10)
                    {
                         cout << " et y: 0" << j;
                    }
                    else
                    {
                         cout << " et y: " << j;
                    }
               }
               cout << "     et est de coordonnée         x: ";
               if (Jeu_Terrain.tab_Terrain_Cellule[(i * Jeu_Terrain.DIMY) + j].getPositionCellule().x < 10)
               {
                    cout << "0";
               }
               cout << Jeu_Terrain.tab_Terrain_Cellule[(i * Jeu_Terrain.DIMY) + j].getPositionCellule().x
                    << " et y: ";
               if (Jeu_Terrain.tab_Terrain_Cellule[(i * Jeu_Terrain.DIMY) + j].getPositionCellule().y < 10)
               {
                    cout << "0";
               }

               cout << Jeu_Terrain.tab_Terrain_Cellule[(i * Jeu_Terrain.DIMY) + j].getPositionCellule().y << "  et a comme État: " << Jeu_Terrain.tab_Terrain_Cellule[(i * Jeu_Terrain.DIMY) + j].getC_Etat()
                    << '\n';
          }
          cout << '\n';

          AffichageTerrainSimpleTest();

          sleep(3);

          cout << '\n'
               << "Test de la fonction d'ajout de mur une colone sur 2 " << Jeu_Terrain.DIMX - 1
               << '\n'
               << '\n';

          for (unsigned int i = 0; i < Jeu_Terrain.DIMX; i++)
          {
               un_vecteur.x = i;
               for (unsigned int j = 0; j < Jeu_Terrain.DIMY; j = j + 2)
               {
                    un_vecteur.y = j;
                    Jeu_Terrain.AjouteMur(un_vecteur);
               }
          }

          AffichageTerrainSimpleTest();

          cout << "Test d'Ajout Nourriture sur une ligne sur deux: " << '\n'
               << '\n';

          for (unsigned int i = 0; i < Jeu_Terrain.DIMX; i = i + 2)
          {
               un_vecteur.x = i;
               for (unsigned int j = 0; j < Jeu_Terrain.DIMY; j = j + 1)
               {
                    un_vecteur.y = j;
                    Jeu_Terrain.AjouteNourriture(un_vecteur);
               }
          }

          cout << '\n';

          AffichageTerrainSimpleTest();

          sleep(3);

          cout << '\n'
               << "Test de la fonction d'ajout de mur une colone sur 2 " << Jeu_Terrain.DIMX - 1
               << '\n'
               << '\n';

          for (unsigned int i = 0; i < Jeu_Terrain.DIMX; i++)
          {
               un_vecteur.x = i;
               for (unsigned int j = 0; j < Jeu_Terrain.DIMY; j = j + 2)
               {
                    un_vecteur.y = j;
                    Jeu_Terrain.AjouteMur(un_vecteur);
               }
          }

          AffichageTerrainSimpleTest();

          sleep(3);
          // Jeu

          cout << "********************" << '\n'
               << '\n'
               << "Partie Jeu :" << '\n'
               << '\n';
          cout << "Reformation du Terrain par un Terrain 20*20 et affichage avec Fourmi :" << '\n'
               << '\n';

          Terrain alpha(20, 20);
          Jeu_Terrain = alpha;

          AffichageTerrainTest();

          sleep(2);

          cout << "================================" << '\n'
               << '\n'
               << '\n'
               << "Essaye de l'actualisation de la Partie :" << '\n'
               << '\n';

          unsigned int fin_de_test = 1;
          do
          {
               for (unsigned int i = 0; i < fin_de_test; i++)
               {
                    ActualisationPartie();
                    AffichageTerrainTest();
                    sleep(1);
               }

               cout << '\n'
                    << "Voulez vous continuez le test d'actualisation ?(entrer un nombre d'actualisation (max 10))" << '\n'
                    << '\n';
               cin >> fin_de_test;
               cout << '\n';
               if (fin_de_test == 0)
               {
                    break;
               }
               if (fin_de_test > 10)
               {
                    fin_de_test = 10;
               }

          } while (fin_de_test > 0);

          ActualisationPartie();
          AffichageTerrainTest();
          cout << '\n'
               << "Sur le terrain dessus :"
               << '\n';
          int j;
          for (j = Jeu_Terrain.Fourmiliere.tabFourmi.size() - 1; j >= 0; j--)
          {
               if (Jeu_Terrain.Fourmiliere.tabFourmi.size() > 0)
               {
                    if (Jeu_Terrain.Fourmiliere.tabFourmi[j].p_nourriture)
                    {
                         cout << '\n'
                              << "La fourmi en x:" << Jeu_Terrain.Fourmiliere.tabFourmi[j].getPosition().x << " et y:" << Jeu_Terrain.Fourmiliere.tabFourmi[j].getPosition().y << " porte de la nourriture" << '\n'
                              << '\n';
                    }

                    Jeu_Terrain.AjouteMur(Jeu_Terrain.Fourmiliere.tabFourmi[j].getPosition());
               }
          }

          cout << '\n'
               << "Affichage du terrain après une tentative d'écrasement des Fourmi et une actualisation" << '\n';
          AffichageTerrainTest();
          cout << '\n'
               << "Le tableau de fourmi est de taille " << Jeu_Terrain.Fourmiliere.tabFourmi.size()
               << '\n'
               << " Le nombre de fourmi restante est de " << Jeu_Terrain.Fourmiliere.getNombreFourmis()
               << '\n'
               << '\n';

          sleep(5);

          cout << "Test d'ajout de nourriture sur une nouvelle fourmi" << '\n'
               << '\n';

          Jeu_Terrain.NaissanceFourmi();
          do
          {
               ActualisationPartie();
               AffichageTerrainTest();
               sleep(2);
          } while (Jeu_Terrain.getEtat(Jeu_Terrain.Fourmiliere.tabFourmi[0].getPosition().x, Jeu_Terrain.Fourmiliere.tabFourmi[0].getPosition().y) == 0);

          Jeu_Terrain.AjouteNourriture(Jeu_Terrain.Fourmiliere.tabFourmi[0].getPosition());

          AffichageTerrainTest();

          cout << '\n'
               << '\n'
               << "Fin de test de Regression"
               << '\n'
               << '\n';
     }
}
