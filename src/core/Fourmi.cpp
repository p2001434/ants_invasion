/**
 * @file Fourmi.cpp
 * @brief Définition des fonctions et procédures de Fourmi.h
 * @date 2023-04-30
 */
#include "Fourmi.h"
#include <iostream>
#include <cstdlib>

Fourmi::Fourmi(const Vecteur &position_colonie)
{
    position.x = position_colonie.x;
    position.y = position_colonie.y;

    p_nourriture = false;
}

const Vecteur &Fourmi::getPosition() const
{
    return position;
}

void Fourmi::setPosition(const Vecteur &nouveau_vecteur)
{
    position.x = nouveau_vecteur.x;
    position.y = nouveau_vecteur.y;
}
