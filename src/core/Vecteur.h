/**
 * @brief Module définissant un Vecteur
 * @file Vecteur.h
 * @date 2023/03/05
 */

#ifndef _VECTEUR_H
#define _VECTEUR_H

/**
 * @brief Un Vecteur est composé d'un entier x et d'un entier y.
 */
struct Vecteur
{
public:

    /**
     * @brief coordonnée en x du Vecteur
     * 
     */
    unsigned int x; 

    /**
     * @brief coordonnée en y du Vecteur
     * 
     */
    unsigned int y; 

    //////////////////////////////////////////////////////

    /**
     * @fn Vecteur()
     * @brief Constructeur d'un Vecteur par défaut (x,y)=(0,0).
     */
    Vecteur();

    //////////////////////////////////////////////////////

    /**
     * @fn Vecteur(const unsigned int u,const unsigned int v)
     * @brief Constructeur par copie d'un Vecteur par défaut (x,y)=(u,v).
     * @param[in]: u, un entier constant non signé.
     * @param[in]: v, un entier constant non signé.
     */
    Vecteur(const unsigned int u,const unsigned int v);

    //////////////////////////////////////////////////////

    /**
     * @fn bool operator==(const Vecteur B) const
     * @brief Fonction qui teste si 2 vecteurs sont égaux.
     * @param[in] : B , un Vecteur B.
     * @return Retourne un booléen vrai ou faux, indiquant si oui ou non si 2 vecteurs sont égaux.
     */
    bool operator==(const Vecteur B) const;

    //////////////////////////////////////////////////////
    
    /**
     * @fn Vecteur operator+(const Vecteur B)
     * @brief Fonction qui permet de faire l'addition en 2 Vecteur.
     * @param[in] : B , un Vecteur constant B.
     * @return Retourne un Vecteur, résultant de l'addition de 2 Vecteur.
     */
    Vecteur operator+(const Vecteur B);

    //////////////////////////////////////////////////////

    /**
     * @fn Vecteur operator-(const Vecteur B)
     * @brief Fonction qui permet de faire la soustraction de 2 Vecteur.
     * @param[in] : B , un Vecteur non modifiable B
     * @return Retourne un Vecteur, résultant de la soustraction de 2 Vecteur.
     */
    Vecteur operator-(const Vecteur B);

    //////////////////////////////////////////////////////
};

#endif


/* Documentation tag for Doxygen
 */

/*! \mainpage Ants Invasion Documentation
 * 
 * \section intro_sec Introduction
 *
 * Voici la documentation de Ants Invasion. Simulation créée sous le contexte d'un projet en conception <br>
 * et développementd'application (LIFAPCD). Le projet est codé en C/C++. Ants Invasion signifit <br>
 * en français "Invasion de fourmis". Cette simulation consiste à observer une colonie de fourmis ayant <br>
 * pour but de se ravitailler en nourriture tout en évitant des obstacles.<br>
 *
 * \section Fonctionnalités
 * 
 * AffichageGraphique::AffichageGraphique(const unsigned int taille_fenêtre) : permet d'initialiser la simulation. <br>
 * AffichageGraphique::AffichageSDL() : permet de lancer et de faire tourner la simulation.
 * 
 * \section Prérequis 
 * 
 * Utilisation des bibliothèques SDL et SDL2.
 * 
 * \subsection installations Installations de SDL et SDL2
 * Tutoriel d'installation sous ubuntu des bibliothèques SDL et SDL2 : https://doc.ubuntu-fr.org/sdl
 *
 * \section commandes Commandes sous Linux
 * - Compilation sous Linux avec la commande : <br>
 * make <br>
 * <br>
 * - Liaisons et créations des fichiers .o et des répertoires bin et obj: <br>
 * make all <br>
 * <br>
 * - Supression des .o et des exécutables : <br>
 * make clean <br>
 * <br>
 * - Exécution de l'affichage graphique : <br>
 * ./bin/mainSDL2 
 * <br>
 * <br>
 * - Exécution de l'affichage texte : <br>
 * ./bin/mainTxt <br>
 * <br>
 * - Exécution du test de régression : <br>
 * ./bin/mainRegression <br>
 * <br>
 * - Doxygen : <br>
 * doxygen doc/docu.doxy <br>
 * firefox doc/html/index.html
 * <br>
 * 
 * \section Utilisation
 * Sur la fenêtre d'accueil , appuyer sur n'importe quelle touche pour lancer la fenêtre de simulation. <br>
 * <br>
 * Une fois la simulation lancée vous pouvez : <br>
 * <br>
 * Accélérer la simulation : Presser sur la touche 1 <br>
 * Ralentir la simulation : Presser sur la touche 2 <br>
 * <br>
 * Passer en mode ajout de mur : Presser sur la touche 3 <br>
 * Passer en mode ajout de nourriture : Presser sur la touche 4 <br>
 * Enlever le mode en cours : Presser sur la touche 5 <br>
 * <br>
 * Redémarrer la partie : Presser sur la touche 6 <br>
 * <br>
 * Quitter la Simulation : Presser sur la touche Echap <br>
 * <br>
 * 
 * \section feuille Feuille de Route
 * - Ajout de prédateur(s).
 * - Amélioration du déplacement des Fourmis.
 * - Changement graphique par saisons
 * 
 * \section fin Auteurs et Remerciements 
 * \subsection auteurs Auteurs :
 * MAO Manita P2001434 <br>
 * MECHICHE Nessim P2004503 <br>
 *
 * \subsection merci Remerciements à :
 * LOUVET Nicolas <br>
 * PRONOST Nicolas <br>
 * MEYER Alexandre <br>
 * <br>
 *
 * \subsection lien Lien UE Printemps 2023 :
 * https://perso.liris.cnrs.fr/ameyer/public_html/www/doku.php?id=lifap4
 * <BR><BR>
 *
 */