/**
 * @file Vecteur.cpp
 * @brief Définition des fonctions et procédures de Vecteur.h
 * @date 2023-03-30
 */

#include "Vecteur.h"

Vecteur::Vecteur()
{
    x = 0;
    y = 0;
}

Vecteur::Vecteur(const unsigned int u,const unsigned int v)
{
    x = u;
    y = v;
}

bool Vecteur::operator==(const Vecteur B) const
{
    return (x == B.x && y == B.y);
}

Vecteur Vecteur::operator+(const Vecteur B)
{
    Vecteur C;
    C.x = x + B.x;
    C.y = y + B.y;
    return C;
}

Vecteur Vecteur::operator-(const Vecteur B)
{
    Vecteur C;
    C.x = x - B.x;
    C.y = y - B.y;
    return C;
}
