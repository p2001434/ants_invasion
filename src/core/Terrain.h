/**
 * @brief Module définissant un Terrain
 * @file Terrain.h
 * @date 2023/03/06
 */

#ifndef _TERRAIN_H
#define _TERRAIN_H

#include "Vecteur.h"
// #include "TableauDynamique.h"
#include "Cellule.h"
#include "Colonie_Fourmis.h"
#include <vector>

using namespace std;

/**
 * @brief Un Terrain est composé d'un vector vers des Cellule, d'un Vecteur pour ses dimensions et d'une Fourmilière.
 */
class Terrain
{
private:
  
  /**
   * @name Configuration des valeurs de base :
   * @{
   */

  /**
   * @brief Valeur pour laquelle une nouvelle Fourmi apparaît 
   */
  static const unsigned int MAX_NOURRITURE = 10;
  /**
   * @brief Valeur représentant le nombre maximum de Fourmi pouvant être présente
   */
  static const unsigned int NB_MAX_FOURMI = 30;

  /**
   * @}
   */
public:

/**
 * @brief Dimension en X du Terrain (hauteur)
 */
  unsigned int DIMX;
/**
 * @brief Dimension en Y du Terrain (largeur)
 */
  unsigned int DIMY;        
  /**
   * @brief Un vector Cellule contenant toutes les Cellule du Terrain
   */
  vector<Cellule> tab_Terrain_Cellule; 

/**
 * @brief Une Fourmilière sur le Terrain de type Colonie
 */
  Colonie Fourmiliere;

  //////////////////////////////////////////////////////

  /**
   * @fn Terrain(const unsigned int dimX, const unsigned int dimY)
   * @brief initialise une Colonie à la coordonnée : FUTURE X ET FUTURE Y avec un nombre de Fourmi qui lui sera assigné. 
   * Initialise toutes les cellules du Jeu avec les bonnes couleurs.
   * Initialise une position aléatoire de la Colonie.
   * Initialise une position aléatoire de la nourriture.
   * Initialise le compteur de nourriture à 0.
   * Le bord du Terrain sera composé uniquement de MUR.
   * @param[in] dimX : entier positif, représentant la taille X de la fenêtre.
   * @param[in] dimY : entier positif, représentant la taille Y de la fenêtre.
   */
  Terrain(const unsigned int dimX, const unsigned int dimY);

  //////////////////////////////////////////////////////

  /**
   * @fn unsigned int getEtat(int x, int y)
   * @brief permet d'avoir l'état d'une Cellule grâce aux coordonnées x et y
   * @param[in] x : entier positif constant, représentant la coordonnée x de la cellule
   * @param[in] y : entier positif constant, représentant la coordonnée y de la cellule
   * @return Retourne un entier non signé representant l'état d'une cellule.
   */
  const unsigned int getEtat(const unsigned int x,const unsigned int y)const;

  //////////////////////////////////////////////////////


  /**
   * @fn void AjouteNourriture(const Vecteur &position_souris)
   * @brief Change l'état d'une Cellule Vide désignée par un Vecteur position_souris en l'état NOURRITURE.
   * Si il y autre chose alors l'Etat ne change pas
   * @param[in] position_souris : un Vecteur constant
   */
  void AjouteNourriture(const Vecteur &position_souris);

  //////////////////////////////////////////////////////

  /**
   * @fn void AjouteMur(const Vecteur &position_souris)
   * @brief Change l'état d'une Cellule désignée par un Vecteur position_souris en l'état MUR 
   * Si ce n'est pas une Cellule Colonie
   * @param[in] position_souris : un Vecteur constant
   */
  void AjouteMur(const Vecteur &position_souris);

  //////////////////////////////////////////////////////


  /**
   * @fn void NaissanceFourmi()
   * @brief Procédure permettant la naissance d'une Fourmi.
   * Dans le cas où le compteur_nourriture est strictement  supérieur à MAX_NOURRITURE et 
   * le nombre de fourmi de la fourmilière soit strictement inférieur à NOURRITURE
   */
  void NaissanceFourmi();

  //////////////////////////////////////////////////////

  /**
   * @fn void LâcherPheromone(const unsigned int indice_fourmi)
   * @brief Procédure permettant de relâcher des phéromones sur des Cellule.
   * @param[in] indice_fourmi : entier positif constant
   */
  void LacherPheromone(const unsigned int indice_fourmi);

  //////////////////////////////////////////////////////

  /**
   * @fn void NettoyagePheromone()
   * @brief Procédure permettant de décrémenter de -1 la quantité de phéromone de toutes les Cellule.
   */
  void NettoyagePheromone();

  //////////////////////////////////////////////////////

  /**
   * @fn void DeplacementFourmi(unsigned int indice_fourmi);
   * @brief Procédure permettant de déplacer des fourmis de manière aléatoire si cela est possible.
   * @param[in] indice_fourmi : entier positif constant
   */
  void DeplacementFourmi(const unsigned int indice_fourmi);

  //////////////////////////////////////////////////////

  /**
   * @fn void DeplacementAleatoire(const vector<Cellule> &tableau_de_case_a_trier, const unsigned int indice_de_la_fourmi)
   * @brief Procédure permettant de choisir une Cellule où se déplacera la Fourmi.
   * Si elle porte de la nourriture elle ira sur une Cellule contenant de la phéromone maison
   * Sinon elle ira sur une Cellule contenant de la phéromone nourriture.
   * @param[in] tableau_de_case_a_trier : un vector Cellule constant
   * @param[in] indice_de_la_fourmi : entier positif constant, représentant l'indice de la Fourmi.
   */
  void DeplacementAleatoire(const vector<Cellule> &tableau_de_case_a_trier, const unsigned int indice_de_la_fourmi);

  //////////////////////////////////////////////////////

  /**
   * @fn void SuivreChemin(const unsigned int i)
   * @brief Procédure permettant de remplir un tableau de Cellule disponibles autour de la Fourmi.
   * Si on trouve de la phéromone dans ce tableau, on appelle la procédure DeplacementAleatoire.
   * @param[in] i : un entier positif constant
   */
  void SuivreChemin(const unsigned int i);

  //////////////////////////////////////////////////////

   /**
   * @fn const unsigned int AttrapeNourriture(const unsigned int IndiceFourmi)
   * @brief Si la Fourmi ne porte pas de nourriture, elle prendra de la NOURRITURE à proximité d'elle.
   * Si la Fourmi est à proximité de la Colonie, elle déposera la nourriture qu'elle porte.
   * @param[in] IndiceFourmi : entier positif constant
   * @return un entier positif constant : 1 pour avoir attraper / jeter la nourriture , 0 pour avoir rien fait.
   */
  const unsigned int AttrapeNourriture(const unsigned int IndiceFourmi);

  //////////////////////////////////////////////////////

   /**
   * @fn  void MouvementDesFourmis()
   * @brief Procédure permettant de déplacer toutes les Fourmi en appelant SuivreChemin si AttrapeNourriture nous retourne 0.
   */
  void MouvementDesFourmis();

  //////////////////////////////////////////////////////


};


#endif