/**
 * @brief Module définissant l'AffichageGraphique
 * @file AffichageGraphique.h
 * @date 2023-03-06
 */

#ifndef _AFFICHAGE_GRAPHIQUE_H
#define _AFFICHAGE_GRAPHIQUE_H

#include "../core/Jeu.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL_image.h>

/**
 * @brief AffichageGraphique est représenté par un Jeu , d'un lien SDL, une taille de pixel et une taille de fenêtre x et y.
 *
 */
class AffichageGraphique
{

public:
        //////////////////////////////////////////////////////

        /**
         * @fn AffichageGraphique(const unsigned int taille_de_la_fenetre_x_et_y )
         * @brief Constructeur d'un AffichageGraphique
         * Va verifier que la valeur d'entrée n'est pas absurde puis initialise
         * toutes les variable de AffichageGraphique et de son Jeu .
         * @param[in] taille_de_la_fenetre_x_et_y : entier positif constant
         */
        AffichageGraphique(const unsigned int taille_de_la_fenetre_x_et_y);

        //////////////////////////////////////////////////////

        /**
         * @fn void AffichageSDL();
         * @brief Procédure permettant l'Affichage du Jeu avec SDL/SDL2
         * Initialise correctement SDL, Boucle l'Affichage , et libère proprement SDL.
         */
        void AffichageSDL();

        //////////////////////////////////////////////////////

private:
        /**
         * @name Valeur de configuration de base :
         * @{
         *
         */
        /**
         * @brief Valeur de la Dimension en X et de la Dimension en Y de la Taille Du Jeu et du Terrain
         */
        static const unsigned int TAILLE_TERRAIN_DE_BASE = 40;
        /**
         * @}
         */

        //////////////////////////////////////////////////////

        /**
         * @brief enum type Couleur_herbe représentant une Couleur par un entier
         */
        enum class Couleur_herbe
        {
                VERT_MOCHE, /**< enum de type Couleur_herbe de valeur 0*/
                VERT_KAKI,  /**< enum de type Couleur_herbe de valeur 1*/
                VERT_CLAIR, /**< enum de type Couleur_herbe de valeur 2*/
                VERT_DURE,  /**< enum de type Couleur_herbe de valeur 3*/
                VERT_FONCE  /**< enum de type Couleur_herbe de valeur 4*/
        };

        //////////////////////////////////////////////////////

        /**
         * @brief enum Type Mode_souris pour représenter l'état du clic pour AjoutElement()
         *
         */
        enum Mode_souris
        {
                RIEN,
                AJOUT_MUR,
                AJOUT_NOURRITURE
        };

        //////////////////////////////////////////////////////

        /***
         * @name Membre pour faire fonctionner SDL2 :
         * @{
         */
        /**
         * @brief Pointeur vers la fenêtre SDL.
         */
        SDL_Window *m_window;

        /**
         * @brief Pointeur vers le tampon SDL.
         */
        SDL_Renderer *m_renderer;

        /**
         * @brief Pointeur vers la texture de l'écran d'accueil.
         */
        SDL_Texture *m_texture_accueil;

        /**
         * @brief Pointeur vers la texture de la nourriture.
         */
        SDL_Texture *m_texture_n;

        /**
         * @brief Pointeur vers la texture d'un mur.
         */
        SDL_Texture *m_texture_m;

        /**
         * @brief Pointeur vers la texture d'un bloc de Colonie.
         */
        SDL_Texture *m_texture_c;

        /**
         * @brief Pointeur vers la texture d'une Fourmi neutre.
         */
        SDL_Texture *m_texture_fun;

        /**
         * @brief Pointeur vers la texture d'une Fourmi portant de la nourriture.
         */
        SDL_Texture *m_texture_f_deux;

        /**
         * @brief un Rectangle SDL2.
         */
        SDL_Rect rec;
        /**
         * @brief entier représentant la taille d'un pixel, va être utilisé pour afficher des images 
         * ou dessiner des pixels de couleur
         */
        int taille_pixel;
        /**
         * @}
         */

        /**
         * @brief Jeu qui va être affiché par AffichageSDL()
         */
        Jeu Jeu_Graphique;

        /**
         * @brief entier positif représentant la longueur d'une ligne du Terrain
         */
        unsigned int taille_ligne;
        /**
         * @brief entier positif représentant la hauteur d'une colone du Terrain
         */
        unsigned int taille_colone;

        /**
         * @brief Entier positif représentant la Hauteur de la fenêtre SDL2
         */
        unsigned int TAILLE_FX; 
        /**
         * @brief Entier positif représentant la Largeur de la fenêtre SDL2
         * 
         */
        unsigned int TAILLE_FY; 

        //////////////////////////////////////////////////////

        /**
         * @fn void InitFenetre()
         * @brief Initialise l'environnement de SDL/SDL2 en vérifiant
         * les erreurs possibles
         */
        void InitFenetre();

        //////////////////////////////////////////////////////

        /**
         * @fn void AffichageEcranDemarrage()
         * @brief Simple affichage d'une fenêtre SDL2 avec une image d'écran de démarrage.
         */
        void AffichageEcranDemarrage();

        //////////////////////////////////////////////////////

        /**
         * @fn void AjouteElement(const int mode, const int position_x,const int position_y)
         * @brief Procédure d'ajout d'un mur ou de nourriture dans le Jeu sur une Cellule
         * a des coordonnées x et y.
         * @param[in] mode : entier constant
         * @param[in] position_x : entier positif constant
         * @param[in] position_y : entier positif constant
         */
        void AjouteElement(const int mode, const int position_x, const int position_y);

        //////////////////////////////////////////////////////

        /**
         * @fn bool DessineC(const unsigned int etat_cellule,const Cellule & une_cellule)
         * @brief Fonction d'Affichage des Cellule en fonction de leur état et de leurs Pheromones.
         * @param[in] etat_cellule : entier positif constant
         * @param[in] une_cellule : Référence vers une Cellule Constant
         * @return un booléen constant qui est faux si tout c'est bien passé
         */
        const bool DessineC(const unsigned int etat_cellule, const Cellule &une_cellule);

        //////////////////////////////////////////////////////

        /**
         * @fn bool DessineF(const bool porte_de_la_nourriture, const Vecteur & coordonne)
         * @brief  Fonction d'Affichage des Fourmi en fonction de leurs états.
         * @param[in] porte_de_la_nourriture : un booléen constant
         * @param[in] coordonne : Référence vers un Vecteur constant
         * @return retourne un booléen constant , faux si tout s'est bien passé.
         */
        const bool DessineF(const bool porte_de_la_nourriture, const Vecteur &coordonne);

        //////////////////////////////////////////////////////

        /**
         * @fn void AffichageBoucle()
         * @brief Procédure d'Affichage du Jeu avec SDL étant interactif et s'actualisant régulièrement
         * en Appelant DessineF , DessineC
         */
        void AffichageBoucle();

        //////////////////////////////////////////////////////

        /**
         * @fn const bool AffichageInitImage()
         * @brief Fonction chargeant toutes les images de /data dans les textures de la classe
         * @return un booléen constant qui est vrai si tout c'est bien passé.
         */
        const bool AffichageInitImage();

        //////////////////////////////////////////////////////

        /**
         * @fn void AffichageDestruction()
         * @brief Procédure de désallocation et de destruction des éléments de SDL2
         */
        void AffichageDestruction();

        //////////////////////////////////////////////////////

        /**
         * @fn void DestructionPropreTexture()
         * @brief Procédure de désallocation et destruction des Textures.
         */
        void DestructionPropreTexture();

        //////////////////////////////////////////////////////
};

#endif
