/**
 * @file AffichageGraphique.cpp
 * @brief Définition des fonctions et procédures de AffichageGraphique.h
 * @date 2023-04-30
 */

#include <iostream>
#include "AffichageGraphique.h"
#include <time.h>
#include <unistd.h>
using namespace std;

AffichageGraphique::AffichageGraphique(const unsigned int taille_fx) : Jeu_Graphique(TAILLE_TERRAIN_DE_BASE, TAILLE_TERRAIN_DE_BASE)
{
    unsigned int taille_de_la_fenetre = taille_fx;
    if (!((taille_de_la_fenetre > 100) && (taille_de_la_fenetre <= 2000)))
    {
        taille_de_la_fenetre = 500;
    }
    
    TAILLE_FX = taille_de_la_fenetre;               // hauteur de la fenêtre
    TAILLE_FY = taille_de_la_fenetre;               // largeur de la fenêtre
    taille_colone = Jeu_Graphique.Jeu_Terrain.DIMY; // taille d'une colone
    taille_ligne = Jeu_Graphique.Jeu_Terrain.DIMX;  // longueur d'une ligne
    m_window = nullptr;
    m_renderer = nullptr;

    m_texture_n = nullptr;

    m_texture_m = nullptr;

    m_texture_c = nullptr;

    m_texture_fun = nullptr;

    m_texture_f_deux = nullptr;
    // obliger de faire un taille pixel X*x ou y*y pour éviter les problèmes de distortion d'image
    taille_pixel = min((TAILLE_FY / taille_colone), (TAILLE_FX / taille_ligne));
    rec.h = TAILLE_FX;
    rec.w = TAILLE_FY;
}

void AffichageGraphique::AffichageSDL()
{
    InitFenetre();
    if (AffichageInitImage())
    {
        AffichageEcranDemarrage();
        AffichageBoucle();
        AffichageDestruction();
    }
}

void AffichageGraphique::InitFenetre()
{

    if (SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        cout << "Erreur lors de l'initialisation de la SDL : " << SDL_GetError() << endl;
        SDL_Quit();
        exit(1);
    }

    if (TTF_Init() != 0)
    {
        cout << "Erreur lors de l'initialisation de la SDL_ttf : " << TTF_GetError() << endl;
        SDL_Quit();
        exit(1);
    }

    int imgFlags = IMG_INIT_PNG | IMG_INIT_JPG;
    if (!(IMG_Init(imgFlags) & imgFlags))
    {
        cout << "Erreur lors de l'initialisation de imgFlags SDL" << IMG_GetError() << endl;
        SDL_Quit();
        exit(1);
    }
    m_window = SDL_CreateWindow("fenêtre_de_simulation", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, TAILLE_FY, TAILLE_FX, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
    if (m_window == nullptr)
    {
        cout << "Erreur 01 : lors de l’initialisation de la fenêtre";
        AffichageDestruction();
    }

    m_renderer = SDL_CreateRenderer(m_window, -1, SDL_RENDERER_ACCELERATED);
    if (m_renderer == nullptr)
    {
        cout << "Erreur  02 : lors de l'affichage de la fenêtre";
        AffichageDestruction();
    }
}

void AffichageGraphique::AffichageEcranDemarrage()
{

    rec.x = 0;
    rec.y = 0;
    SDL_Event events;
    bool quit = false;

    while (!quit)
    {

        // tant que l’arrêt n'est pas demandé
        SDL_SetRenderDrawColor(m_renderer, 125, 125, 125, 56);
        SDL_RenderClear(m_renderer);

        SDL_RenderCopy(m_renderer, m_texture_accueil, nullptr, &rec);

        // tant qu'il y a des événements à traiter (cette boucle n'est pas bloquante)
        while (SDL_PollEvent(&events))
        {
            if ((events.type == SDL_QUIT) || (events.type == SDL_KEYDOWN) || (events.type == SDL_MOUSEBUTTONDOWN))
            {
                quit = true;
            }
        }
        SDL_RenderPresent(m_renderer); // va swap les renderer
    }
}

const bool AffichageGraphique::AffichageInitImage()
{
    bool action = true;
    string tab[6] = {"mur.jpg", "colonie.jpg", "nourriture.png", "FourmiQuiPorte.png", "FourmiSimple.png", "Ants_invasion.png"};
    SDL_Surface *tabS[6];
    SDL_Texture *tabT[6];

    for (unsigned int i = 0; i < 6; i++)
    {
        tabS[i] = IMG_Load(("data/" + tab[i]).c_str()); //(c_str() car IMG_Load a besoin d'un pointeur)
        if (tabS[i] == nullptr)
        {
            cout << "Erreur lors du chargement de la surface de " << tab[i] << endl;
            return !(action);
        }

        tabT[i] = SDL_CreateTextureFromSurface(m_renderer, tabS[i]);
        if (tabT[i] == nullptr)
        {
            cout << "Erreur lors du chargement de la texture de " << tab[i] << endl;
            SDL_FreeSurface(tabS[i]);
            return !(action);
        }
    }
    m_texture_m = tabT[0];
    m_texture_c = tabT[1];
    m_texture_n = tabT[2];
    m_texture_fun = tabT[3];
    m_texture_f_deux = tabT[4];
    m_texture_accueil = tabT[5];

    for (unsigned int i = 0; i < 6; i++)
    {
        SDL_FreeSurface(tabS[i]);
    }

    return action;
}

const bool AffichageGraphique::DessineC(const unsigned int etat_cellule, const Cellule &la_cellule)
{

    bool action = true;

    // ajouter le téléchargement de l'image dans la texture
    rec.x = la_cellule.getPositionCellule().x * taille_pixel;
    rec.y = la_cellule.getPositionCellule().y * taille_pixel;
    int val_couleur_herbe = la_cellule.getCouleur();
    if (etat_cellule == 0) // si c'est vide je dessine un pixel vert
    {

        switch (val_couleur_herbe)
        {
        case (int)Couleur_herbe::VERT_MOCHE:
            SDL_SetRenderDrawColor(m_renderer, 124, 252, 0, 255);
            break;

        case (int)Couleur_herbe::VERT_KAKI:
            SDL_SetRenderDrawColor(m_renderer, 127, 255, 0, 255);
            break;

        case (int)Couleur_herbe::VERT_CLAIR:
            SDL_SetRenderDrawColor(m_renderer, 154, 205, 50, 255);
            break;

        case (int)Couleur_herbe::VERT_DURE:
            SDL_SetRenderDrawColor(m_renderer, 173, 255, 47, 255);
            break;

        case (int)Couleur_herbe::VERT_FONCE:
            SDL_SetRenderDrawColor(m_renderer, 152, 251, 152, 255);
            break;

        default:
            cout << "erreur lors du switch vert de Dessine C  " << etat_cellule << "  couleur  : " << val_couleur_herbe << endl;
            action = false;
            break;
        }
        SDL_RenderFillRect(m_renderer, &rec);
        if ((la_cellule.getPheromone_maison() != 0) || (la_cellule.getPheromone_nourriture() != 0))
        {
            SDL_SetRenderDrawColor(m_renderer, 0, 0, (Uint8)la_cellule.getPheromone_maison(), 255);
            SDL_Rect maison = rec;
            maison.w = rec.w / 2;
            maison.h = rec.h / 2;
            maison.x = rec.x + (taille_pixel / 4);
            maison.y = rec.y + (taille_pixel / 4);
            if ((la_cellule.getPheromone_nourriture() != 0))
            {
                SDL_SetRenderDrawColor(m_renderer, la_cellule.getPheromone_nourriture(), 0, 0, 255);
            }
            SDL_RenderFillRect(m_renderer, &maison);
        }
    }
    else
    {
        switch (etat_cellule)
        {
        case Cellule::MUR:
            SDL_RenderCopy(m_renderer, m_texture_m, nullptr, &rec);
            break;
        case Cellule::NOURRITURE:
            SDL_RenderCopy(m_renderer, m_texture_n, nullptr, &rec);
            ;
            break;
        case Cellule::COLONIE:
            SDL_RenderCopy(m_renderer, m_texture_c, nullptr, &rec);
            break;
        default:
            cout << "erreur lors du switch état de Dessine C " << endl;
            action = false;
            break;
        }
    }

    return action;
}

const bool AffichageGraphique::DessineF(const bool porte_de_la_nourriture, const Vecteur &coordonne)
{

    bool action = true;
    rec.x = coordonne.x * taille_pixel;
    rec.y = coordonne.y * taille_pixel;
    if (porte_de_la_nourriture)
    {
        SDL_RenderCopy(m_renderer, m_texture_fun, nullptr, &rec);
    }
    else
    {
        SDL_RenderCopy(m_renderer, m_texture_f_deux, nullptr, &rec);
    }
    return action;
}

void AffichageGraphique::AjouteElement(const int mode, const int position_en_x, const int position_en_y)
{
    Vecteur coordonne;
    coordonne.x = position_en_x;
    coordonne.y = position_en_y;
    if (mode == (int) AffichageGraphique::AJOUT_MUR)
    {
        Jeu_Graphique.Jeu_Terrain.AjouteMur(coordonne);
    }
    else if (mode == (int) AffichageGraphique::AJOUT_NOURRITURE)
    {
        Jeu_Graphique.Jeu_Terrain.AjouteNourriture(coordonne);
    }
}

void AffichageGraphique::AffichageBoucle()
{
    rec.h = taille_pixel;
    rec.w = taille_pixel;
    Uint32 temps_depuis = SDL_GetTicks();
    Uint32 temps_maintenant;
    int mode = (int) AffichageGraphique::RIEN ;
    int sourisX = 0;
    int sourisY = 0;
    SDL_Event events;
    bool quit = false;
    unsigned int temps_avant_refresh = 1150;

    while (!quit)
    {

        // tant que l’arrêt n'est pas demandé
        SDL_SetRenderDrawColor(m_renderer, 12, 255, 34, 56);
        SDL_RenderClear(m_renderer);
        temps_maintenant = SDL_GetTicks();

        // tout les x temps je redessine
        if ((temps_maintenant - temps_depuis) > temps_avant_refresh)
        {

            temps_maintenant = temps_depuis;
            temps_depuis = SDL_GetTicks();
            Jeu_Graphique.ActualisationPartie();
        }

        for (unsigned int i = 0; i < taille_colone; i++)
        {
            for (unsigned int j = 0; j < taille_ligne; j++)
            {
                // va retourner vrai ou faux
                quit = !(DessineC(Jeu_Graphique.Jeu_Terrain.tab_Terrain_Cellule[i * taille_colone + j].getC_Etat(), Jeu_Graphique.Jeu_Terrain.tab_Terrain_Cellule[i * taille_colone + j]));
                if(quit){
                    cout<<"Erreur 1 lors de l'affichage Boucle DessineC";
                    exit(1);
                }
            }
        }

        if (Jeu_Graphique.Jeu_Terrain.Fourmiliere.getNombreFourmis() != 0)
        {
            for (int i = 0; i < (int)Jeu_Graphique.Jeu_Terrain.Fourmiliere.getNombreFourmis(); i++)
            {
                quit = !(DessineF(Jeu_Graphique.Jeu_Terrain.Fourmiliere.tabFourmi[i].p_nourriture, Jeu_Graphique.Jeu_Terrain.Fourmiliere.tabFourmi[i].getPosition()));
                if(quit){
                    cout<<"Erreur 2 lors de l'affichage Boucle Dessine F";
                    exit(1);
                }
            }
        }

        // tant qu'il y a des événements à traiter (cette boucle n'est pas bloquante)
        while (SDL_PollEvent(&events))
        {

            if (events.type == SDL_QUIT)
                quit = true; // Si l'utilisateur a clique sur la croix de fermeture
            else if (events.type == SDL_KEYDOWN)
            {
                switch (events.key.keysym.scancode)
                {
                case SDL_SCANCODE_ESCAPE:
                    quit = true;
                    break;
                case SDL_SCANCODE_1:
                    if (temps_avant_refresh > 100)
                    {
                        temps_avant_refresh = temps_avant_refresh - 50;
                    }
                    break;
                case SDL_SCANCODE_2:
                    if (temps_avant_refresh < 10000)
                    {
                        temps_avant_refresh = temps_avant_refresh + 50;
                    }
                    break;
                case SDL_SCANCODE_3:
                    mode = (int) AffichageGraphique::AJOUT_MUR;
                    break;
                case SDL_SCANCODE_4:
                    mode = (int) AffichageGraphique::AJOUT_NOURRITURE;
                    break;
                case SDL_SCANCODE_5:
                    mode = (int) AffichageGraphique::RIEN;
                    break;
                case SDL_SCANCODE_6:
                    mode = (int) AffichageGraphique::RIEN;
                    Jeu_Graphique.RedemarrerPartie();
                    break;
                default:
                    break;
                }
            }

            else if (events.type == SDL_MOUSEBUTTONDOWN) // si un bouton de la souris est pressé
            {
                if (events.button.button == SDL_BUTTON_LEFT) // si c'est le gauche
                {
                    sourisX = events.button.x;
                    sourisY = events.button.y;
                    AjouteElement(mode, sourisX / taille_pixel, sourisY / taille_pixel);
                }
            }
        }
        SDL_RenderPresent(m_renderer); // va swap les renderer
    }
}

void AffichageGraphique::DestructionPropreTexture()
{

    SDL_DestroyTexture(m_texture_m);
    SDL_DestroyTexture(m_texture_c);
    SDL_DestroyTexture(m_texture_n);
    SDL_DestroyTexture(m_texture_fun);
    SDL_DestroyTexture(m_texture_f_deux);
    SDL_DestroyTexture(m_texture_accueil);
}

void AffichageGraphique::AffichageDestruction()
{
    TTF_Quit();
    DestructionPropreTexture();
    SDL_DestroyRenderer(m_renderer);
    SDL_DestroyWindow(m_window);
    SDL_Quit();
    m_renderer = nullptr;
    m_window = nullptr;
}
