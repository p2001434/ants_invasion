OBJ=obj/Fourmi.o obj/Cellule.o obj/Colonie_Fourmis.o obj/Terrain.o  obj/Vecteur.o obj/Jeu.o 
OBJTXT = obj/AffichageConsole.o obj/mainTxt.o  obj/winTxt.o 
OBJSDL2 = obj/AffichageGraphique.o obj/mainSDL2.o
OBJREGR = obj/mainRegression.o 

EXEC = bin/mainTxt bin/mainRegression bin/mainSDL2

CCP_FLAGS=-DNDEBUG
CCP=g++ -g -Wall $(CCP_FLAGS)
SDL2_FLAGS= -lSDL2 -lSDL2_ttf -lSDL2_image -lSDL2_mixer -lGL

all : bin obj $(OBJ) $(OBJTXT) $(EXEC) 

test : $(OBJ) $(OBJTXT) $(OBJSDL2)


#.o

#.o liaisons entre les classes de la simulation OBJ .Dans le répertoire src/core/

obj/Vecteur.o: src/core/Vecteur.cpp src/core/Vecteur.h
	$(CCP) -o obj/Vecteur.o -c src/core/Vecteur.cpp

obj/Fourmi.o : src/core/Fourmi.cpp src/core/Fourmi.h src/core/Vecteur.h
	$(CCP) -o obj/Fourmi.o -c src/core/Fourmi.cpp

obj/Cellule.o : src/core/Cellule.cpp src/core/Cellule.h
	$(CCP) -o obj/Cellule.o -c src/core/Cellule.cpp

obj/Colonie_Fourmis.o : src/core/Colonie_Fourmis.cpp src/core/Colonie_Fourmis.h src/core/Fourmi.h
	$(CCP) -o obj/Colonie_Fourmis.o -c src/core/Colonie_Fourmis.cpp

obj/Terrain.o : src/core/Terrain.cpp src/core/Terrain.h src/core/Cellule.h src/core/Colonie_Fourmis.h  src/core/Vecteur.h
	$(CCP) -o obj/Terrain.o -c src/core/Terrain.cpp

obj/Jeu.o : src/core/Jeu.cpp src/core/Jeu.h src/core/Terrain.h
	$(CCP) -o obj/Jeu.o -c src/core/Jeu.cpp


# en lien avec l'affichage Txt console OBJTXT. Dans le répertoire src/txt

obj/mainTxt.o : src/txt/mainTxt.cpp src/txt/AffichageConsole.h
	$(CCP) -o obj/mainTxt.o -c src/txt/mainTxt.cpp

obj/AffichageConsole.o : src/txt/AffichageConsole.cpp src/txt/AffichageConsole.h  src/core/Jeu.h src/txt/winTxt.h
	$(CCP) -o obj/AffichageConsole.o -c src/txt/AffichageConsole.cpp

obj/winTxt.o: src/txt/winTxt.cpp src/txt/winTxt.h 
	$(CCP) -o obj/winTxt.o -c src/txt/winTxt.cpp

obj/mainRegression.o : src/txt/mainRegression.cpp src/core/Jeu.h
	$(CCP) -o obj/mainRegression.o -c src/txt/mainRegression.cpp

obj/AffichageGraphique.o : src/SDL2/AffichageGraphique.cpp src/SDL2/AffichageGraphique.h src/core/Jeu.h
	$(CCP) -o obj/AffichageGraphique.o -c src/SDL2/AffichageGraphique.cpp $(SDL2_FLAGS)

 obj/mainSDL2.o : src/SDL2/mainSDL2.cpp src/SDL2/AffichageGraphique.h
	$(CCP) -o obj/mainSDL2.o -c src/SDL2/mainSDL2.cpp $(SDL2_FLAGS)

#.o en lien avec l'affichage SDL2, OBJSDL2. Dans le répertoire src/SDL2

#obj/AffichageGraphique.o: src/SDL2/AffichageGraphique.cpp src/SDL2/AffichageGraphique.h src/core/Jeu.h $(SDL2_FLAGS)
#	$(CPP) -o obj/AffichageGraphique.o -c src/SDL2/AffichageGraphique.cpp $(SDL2_FLAGS)


#executable

bin/mainTxt : obj/mainTxt.o obj/AffichageConsole.o $(OBJ) $(OBJTXT)
	$(CCP) -o bin/mainTxt $(OBJ) $(OBJTXT)

bin/mainRegression: obj/mainRegression.o obj/Jeu.o $(OBJ)
	$(CCP) -o bin/mainRegression $(OBJ) $(OBJREGR) 

bin/mainSDL2 : obj/mainSDL2.o obj/AffichageGraphique.o $(OBJ) $(OBJSDL2) $(SDL2_FLAGS)
	$(CCP) -o bin/mainSDL2 $(OBJ) $(OBJSDL2) $(SDL2_FLAGS)

#nettoyage 
clean:
	rm -f $(OBJ) $(OBJTXT) $(EXEC) $(OBJREGR) $(OBJSDL2)
	

#creation de obj
obj: 
	mkdir obj

#creation de bin

bin:
	mkdir bin

 
	
