## P2001434 MAO Manita 5299  &  P2004503 MECHICHE Nessim 5407

## Nom du projet

Ants_invasion

## Description
Léger programme en c++ pour apprendre. Permettant de simuler grossièrement une petite colonie de Fourmis intelligente cherchant à se ravitailler en nourriture en esquivant les obstacles rencontrés.

## Fonctionnalité 

AffichageGraphique::AffichageGraphique(const unsigned int taille_fenêtre) : permet d'initialisé la simulation.

AffichageGraphique::AffichageSDL() : permet de lancer et de faire tourner la simulation.


## Prérequis 

Bibliothèques SDL et SDL2 installées.


## Installation de SDL et SDL2 sur Linux 

Installation :
https://doc.ubuntu-fr.org/sdl


## Organisation des fichiers :

executable dans le répertoire /bin

fichier objet dans le répertoire /obj

documentation dans le répertoire /doc

image dans le répertoire /data

core du projet dans le répertoire /src/core

fonctionnalité de l'affichage textuel dans le répertoire /src/txt

fonctionnalité de l'affichage SDL2 dans le répertoire /src/SDL2

## Commandes sous Linux

# Compilation sous Linux avec la commande :

make 

# Liaisons entre les fichiers et création des .o :

make all

# Supression des .o et des exécutables :

make clean

# Execution de l'affichage graphique : 

./bin/mainSDL2 

# Execution de l'affichage txt :

./bin/mainTxt

# Execution du test de Regression :

./bin/mainRegression

# Doxygen :

doxygen doc/docu.doxy
firefox doc/html/index.html




## Utilisation

Sur la fenêtre d'accueil , appuyez sur n'importe quelle touche pour lancer la fenêtre de simulation.

Une fois la simulation lancée vous pouvez :

Accéléré la simulation : Pressez 1
Ralentir la simulation : Pressez 2

Passer en mode ajout de mur : Pressez 3
Passez en mode ajout de nourriture : Pressez 4
Enlever le mode en cour : Pressez 5

Redémarrer la Partie : Pressez 6

Quitter la Simulation : Pressez Echap / Barre d'espace



## Feuille de Route 

Ajout de Prédateur, Amélioration du déplacement des Fourmis, Changement graphique par Saison.



## Auteurs et Remerciements

Auteurs :

Mao Manita P2001434
Mechiche Nessim P2004503

Remerciement à :
 
Louvet Nicolas
Pronost Nicolas
Meyer Alexandre



